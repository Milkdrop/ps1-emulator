struct Vector2 {
	i32 x, y;

	Vector2 (i32 _x = 0, i32 _y = 0) { x = _x; y = _y; }
	Vector2& operator= (Vector2 B) { x = B.x; y = B.y; return *this; }
	bool operator== (Vector2 B) const { return x == B.x && y == B.y; }
	Vector2 operator+ (Vector2 B) const { return Vector2 (x + B.x, y + B.y); }
	Vector2 operator- (Vector2 B) const { return Vector2 (x - B.x, y - B.y); }
	Vector2 operator% (Vector2 B) const { return Vector2 (x % ((B.x == 0) ? x : B.x), y % ((B.y == 0) ? y : B.y)); }
	Vector2& operator+= (Vector2 B) { x += B.x; y += B.y; return *this; }
	Vector2& operator-= (Vector2 B) { x -= B.x; y -= B.y; return *this; }
	Vector2& operator%= (Vector2 B) { x %= (B.x == 0) ? x : B.x; y %= (B.y == 0) ? y : B.y; return *this; }
	Vector2 operator* (Vector2 B) { return Vector2 (x * B.x, y * B.y); }
	u32 To32 () { return ((u32) y << 16) | (u16) x; }
};

struct Vector2f {
	float x, y;

	Vector2f (float _x = 0, float _y = 0) { x = _x; y = _y; }
	Vector2f (Vector2 v) { x = v.x; y = v.y; }
	Vector2f& operator= (Vector2f B) { x = B.x; y = B.y; return *this; }
	Vector2f& operator= (Vector2 B) { x = B.x; y = B.y; return *this; }
	Vector2f operator+ (Vector2f B) { return Vector2f (x + B.x, y + B.y); }
	Vector2f operator/ (Vector2f B) { return Vector2f (x / B.x, y / B.y); }
	Vector2f operator/ (Vector2 B) { return Vector2f (x / B.x, y / B.y); }
};

struct Vertex {
	Vector2 point;
	Color color;
	Vector2f texCoords;

	Vertex (Vector2 _point = Vector2 (), Color _color = Color (0xFF808080), Vector2f _texCoords = Vector2f ()) {
		point = _point;
		color = _color;
		texCoords = _texCoords;
	}
};

struct Rect {
	Vector2 topLeft, bottomRight;

	Rect (Vector2 _topLeft = Vector2 (), Vector2 _bottomRight = Vector2 ()) {
		topLeft = _topLeft;
		bottomRight = _bottomRight;
	}
};

static inline Rect CalculateBoundingBox (u8 vSize, Vertex* v) {
	Rect out = Rect (v[0].point, v[0].point);

	for (u8 i = 1; i < vSize; i++) {
		if (v[i].point.x < out.topLeft.x) out.topLeft.x = v[i].point.x;
		if (v[i].point.y < out.topLeft.y) out.topLeft.y = v[i].point.y;
		if (v[i].point.x > out.bottomRight.x) out.bottomRight.x = v[i].point.x;
		if (v[i].point.y > out.bottomRight.y) out.bottomRight.y = v[i].point.y;
	}

	return out;
}

static inline Rect CalculateBoundingBox (u8 vSize, Vector2* v) {
	Rect out = Rect (v[0], v[0]);

	for (u8 i = 1; i < vSize; i++) {
		if (v[i].x < out.topLeft.x) out.topLeft.x = v[i].x;
		if (v[i].y < out.topLeft.y) out.topLeft.y = v[i].y;
		if (v[i].x > out.bottomRight.x) out.bottomRight.x = v[i].x;
		if (v[i].y > out.bottomRight.y) out.bottomRight.y = v[i].y;
	}

	return out;
}