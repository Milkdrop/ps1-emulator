struct Color {
	u8 R, G, B;
	u8 semiTransparent;

	Color (u8 _R = 0x80, u8 _G = 0x80, u8 _B = 0x80, u8 _semiTransparent = 0) {
		R = _R;
		G = _G;
		B = _B;
		semiTransparent = _semiTransparent;
	}

	Color (u32 BGRA, u8 type = COLOR_24BIT) {
		if (type == COLOR_24BIT) {
			R = (BGRA & 0xFF);
			G = (BGRA & 0xFF00) >> 8;
			B = (BGRA & 0xFF0000) >> 16;
			semiTransparent = ((BGRA & 0xFF000000) >> 24) == 0x80;
		} else if (type == COLOR_16BIT) {
			R = (BGRA & 0x1F) << 3;
			G = ((BGRA >> 5) & 0x1F) << 3;
			B = ((BGRA >> 10) & 0x1F) << 3;
			semiTransparent = (BGRA >> 15) != 0;
		}
	}

	Color& operator= (Color colB) { R = colB.R; G = colB.G; B = colB.B; semiTransparent = colB.semiTransparent; return *this; }
	u16 To15Bit () { return (R >> 3) | ((G >> 3) << 5) | ((B >> 3) << 10) | (semiTransparent << 15); }
	u32 To24Bit () { return R | (G << 8) | (B << 16) | ((semiTransparent ? 0x80 : 0xFF) << 24); }
};