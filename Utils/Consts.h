struct Vector2;

const int GLFW_KEY_MAX = 400;

const int RENDER_RAW_TEXTURE = 0b1;
const int RENDER_SEMITRANSPARENT = 0b10;
const int RENDER_TEXTURED = 0b100;
const int RENDER_QUAD_OR_POLY = 0b1000;
const int RENDER_SHADED = 0b10000;

const int LOG_INFO = 0;
const int LOG_WARN = 1;
const int LOG_ERR = 2;
const int LOG_DEBUG = 3;
const int LOG_DEBUG_CPU = 4;
const int LOG_DEBUG_GPU = 5;
const int LOG_DEBUG_MEM = 6;
const int LOG_DEBUG_CDROM = 7;
const int LOG_DEBUG_CTRL = 8;

const int IRQ_VBLANK = 0;
const int IRQ_GPU = 1;
const int IRQ_CDROM = 2;
const int IRQ_DMA = 3;
const int IRQ_TMR0 = 4;
const int IRQ_TMR1 = 5;
const int IRQ_TMR2 = 6;
const int IRQ_CTRL = 7;
const int IRQ_SIO = 8;
const int IRQ_SPU = 9;
const int IRQ_LIGHTPEN = 10;

const int EXCEPT_INT = 0;
const int EXCEPT_ADDR_LOAD = 4;
const int EXCEPT_ADDR_STORE = 5;
const int EXCEPT_INSTR_FETCH = 6;
const int EXCEPT_DATA = 7;
const int EXCEPT_SYSCALL = 8;
const int EXCEPT_BREAK = 9;
const int EXCEPT_RESERVED_INSTR = 0xA;
const int EXCEPT_COPR_UNUSABLE = 0xB;
const int EXCEPT_OVERFLOW = 0xC;

const int VRAM_WIDTH = 1024;
const int VRAM_HEIGHT = 512;

const int TEXCOLORDEPTH_4BIT = 0;
const int TEXCOLORDEPTH_8BIT = 1;
const int TEXCOLORDEPTH_15BIT = 2;

const int COLOR_16BIT = 0;
const int COLOR_24BIT = 1;

const int CTRL_TX_INT = 0;
const int CTRL_RX_INT = 1;

const int GPU_MAX_PACKET_QUEUE = 16;
const int GPU_PARAMFIFO_REQUIREMENT[] = {
//  0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
	0,  0,  2,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  // 0
	0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  // 1
	3,  0,  3,  0,  6,  6,  6,  6,  4,  0,  4,  0,  8,  8,  8,  8,  // 2
	5,  0,  5,  0,  8,  0,  8,  0,  7,  0,  7,  0, 11,  0, 11,  0,  // 3
	2,  0,  2,  0,  0,  0,  0,  0,  2,  0,  2,  0,  0,  0,  0,  0,  // 4
	3,  0,  3,  0,  0,  0,  0,  0,  3,  0,  3,  0,  0,  0,  0,  0,  // 5
	2,  0,  2,  0,  3,  3,  3,  3,  1,  0,  1,  0,  2,  2,  2,  2,  // 6
	1,  0,  1,  0,  2,  2,  2,  2,  1,  0,  1,  0,  2,  2,  2,  2,  // 7
	3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  // 8
	3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  // 9
	3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  // A
	3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  // B
	2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  // C
	2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  // D
	0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  // E
	0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0   // F
};

const int CD_SECTORS_PER_SECOND = 75;
const int CD_BYTES_PER_SECTOR = 2352;