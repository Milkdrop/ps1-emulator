#ifndef GUI_H
#define GUI_H
#include "utils.h"
#include "PSX.h"

class GUI {
public:
	GLFWwindow *GUIContext, *GPUContext;

	GUI () {}
	void Link (PSX* _psx);
	void DrawScreen ();
	void InitGLAndImgui ();

private:
	PSX* psx;
	bool showDebugLogs = false;
	char logFilter[256];

	void DrawDisplay ();
	void DrawVRAM ();
	void DrawCurrentTexture ();
	void DrawStencilTexture ();
	void DrawLogs ();
	void DrawDebugWindows ();

	// Utils
	void DisplayGenericLogWindow (ImGuiTextBuffer &textBuffer, bool* debugSwitch);
	void TextChild (ImGuiTextBuffer &textBuffer);
};

#endif