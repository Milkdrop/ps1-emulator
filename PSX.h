#ifndef PSX_H
#define PSX_H
#include "utils.h"

class PSX;
class CPU;
class GTE;
class GPU;
class GUI;
class MEM;
class Controller;
class CDROM;
class Timer;

#include "Components/MEM.h"
#include "Components/CPU.h"
#include "Components/GTE.h"
#include "Components/GPU.h"
#include "Components/Controller.h"
#include "Components/CDROM.h"
#include "Components/Timer.h"

class PSX {
public:
	PSX (const char* _BIOSFileName, const char* _ROMFileName);
	void Clock ();
	void EnqueueIRQ (u8 code, u32 clockDelay = 0);

	// For debugging, it's best for these to be public
	CPU* cpu;
	GTE* gte;
	GPU* gpu;
	MEM* mem;
	CDROM* cdrom;
	Controller* ctrl;
	Timer* timer;

	bool freeze = 0;
	bool debug = 0;

	volatile u32 clocksThisSecond = 0;
	volatile u32 clocksPreviousSecond = 0;
private:
	// State
	u64 bootTime = 0;
	u32 IRQPending[11];

	// ROMs
	const char* ROMFileName;
	void LoadBIOS (const char* BIOSFileName);
	void LoadROM ();
};

#endif