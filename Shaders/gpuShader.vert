#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec2 aTexCoord;
layout (location = 2) in vec3 aColor;

out vec3 fragColor;
out vec2 texCoord;

uniform ivec2 VRAMSize;

void main () {
	vec2 aPosFix = aPos;

	gl_Position = vec4 ((aPosFix.x / VRAMSize.x) * 2 - 1, (aPosFix.y / VRAMSize.y) * 2 - 1, 0.0, 1.0);
	fragColor = aColor;
	texCoord = aTexCoord;
}