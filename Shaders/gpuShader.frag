#version 330 core
out vec4 FragColor;

#define TEXCOLORDEPTH_4BIT 0
#define TEXCOLORDEPTH_8BIT 1
#define TEXCOLORDEPTH_15BIT 2

layout(pixel_center_integer) in vec4 gl_FragCoord;
in vec2 texCoord;
in vec3 fragColor;

uniform ivec2 textureWindowOffset;
uniform ivec2 textureWindowSize;

uniform ivec2 texpageBase;
uniform ivec2 texpageOffset;
uniform ivec2 textureSize;
uniform ivec2 CLUTLocation;
uniform ivec2 textureFlip;
uniform int textureColorDepth;

uniform bool drawVRAMCopyTexture;
uniform sampler2D VRAM;
uniform sampler2D VRAMCopyTexture;
uniform sampler2D VRAMStencilTexture;

uniform bool textured;
uniform bool renderToMask;
uniform bool semiTransparent;
uniform bool forceBit15;
uniform int semiTransparencyMode;

uniform ivec2 VRAMSize;

vec4 getTextureColor ();
vec4 getPixel (vec2 position);
int pixelTo15Bit (vec4 pixel);

void main () {
	vec4 texColor;
	if (drawVRAMCopyTexture) {
		texColor = texture (VRAMCopyTexture, texCoord);
	} else {
		texColor = getTextureColor ();
	}

	if (renderToMask) {
		if (textured && texColor.w > 0) {
			if (forceBit15 || texColor.w < 1) {
				FragColor = vec4 (1, 1, 1, 1);
			} else {
				discard;
			}
		} else if (!textured && forceBit15) {
			FragColor = vec4 (1, 1, 1, 1);
		} else {
			discard;
		}
	} else {
		vec3 RGB = fragColor.xyz;

		if (textured) {
			FragColor = texColor * vec4 (RGB / 0x80, 1);

			// Make texture opaque
			if (!semiTransparent && texColor.w != 0) {
				FragColor.w = 1;
			}
		} else {
			FragColor = vec4 (fragColor.x / 0xFF, fragColor.y / 0xFF, fragColor.z / 0xFF, 1);
		}

		// Set final alpha
		if (semiTransparent && (!textured || (textured && texColor.w > 0 && texColor.w < 1))) {
			if (semiTransparencyMode == 0) FragColor.w = 0.5f;
			if (semiTransparencyMode == 3) FragColor.w = 0.25f;
		}
	}
}

vec4 getTextureColor () {
	ivec2 textureOffset = ivec2 (textureSize * texCoord * textureFlip);
	if (textureColorDepth == TEXCOLORDEPTH_8BIT) {
		if (textureFlip.x == -1) textureOffset.x += 1;
		textureOffset.x >>= 1;
	} else if (textureColorDepth == TEXCOLORDEPTH_4BIT) {
		if (textureFlip.x == -1) textureOffset.x += 3;
		textureOffset.x >>= 2;
	}
	
	textureOffset += texpageOffset + textureWindowOffset;

	// Get inside texpage's bounding box
	textureOffset -= ivec2 (textureWindowSize * floor (textureOffset / textureWindowSize));

	vec2 positionInTexture = texpageBase + textureOffset;
	vec4 texturePixel = getPixel (positionInTexture);

	if (textureColorDepth == TEXCOLORDEPTH_8BIT) {
		int positionInCLUT = pixelTo15Bit (texturePixel);
		int shamt = 8 * (int (texCoord.x * textureSize.x) & 1);
		if (textureFlip.x == -1) shamt = 8 - shamt;
		texturePixel = getPixel (CLUTLocation + ivec2 ((positionInCLUT & (0xFF << shamt)) >> shamt, 0));

	} else if (textureColorDepth == TEXCOLORDEPTH_4BIT) {
		int positionInCLUT = pixelTo15Bit (texturePixel);
		int shamt = 4 * (int (texCoord.x * textureSize.x) & 3);
		if (textureFlip.x == -1) shamt = 12 - shamt;
		texturePixel = getPixel (CLUTLocation + ivec2 ((positionInCLUT & (0xF << shamt)) >> shamt, 0));
	}

	if (texturePixel == vec4 (0, 0, 0, 1)) texturePixel = vec4 (0, 0, 0, 0);
	return texturePixel;
}

vec4 getPixel (vec2 position) {
	position /= VRAMSize;
	vec4 texturePixel = texture (VRAM, position);
	vec4 stencilPixel = texture (VRAMStencilTexture, position);
	if (stencilPixel != vec4 (0, 0, 0, 1)) texturePixel.w = 0.5f;

	return texturePixel;
}

int pixelTo15Bit (vec4 pixel) {
	pixel *= 0xFF;
	return (int (pixel.x) >> 3) |
		((int (pixel.y) >> 3) << 5) |
		((int (pixel.z) >> 3) << 10) |
		(((pixel.w > 0 && pixel.w < 255) ? 1 : 0) << 15);
}