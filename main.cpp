#include "utils.h"
#include "PSX.h"
#include "GUI.h"

void InputLoop ();
void DebugLoop ();
void KeyCallback (GLFWwindow* window, int key, int scancode, int action, int mods);

GUI* gui;
PSX* psx;

int main (int argc, char* argv[]) {
	if (argc == 1) {
		Log (LOG_ERR, "Usage: %s bios.bin [rom.bin]", argv[0]);
	}
	
	gui = new GUI ();

	if (!glfwInit ()) Log (LOG_ERR, "Cannot initialize GLFW");

	glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	//glfwWindowHint (GLFW_VISIBLE, GLFW_FALSE);
	glfwWindowHint (GLFW_MAXIMIZED, 1);

	GLFWwindow* GPUContext = glfwCreateWindow (VRAM_WIDTH, VRAM_HEIGHT, "", NULL, NULL);
	if (GPUContext == NULL) Log (LOG_ERR, "Cannot create OpenGL window");
	glfwMakeContextCurrent (GPUContext);
	if (glewInit() != GLEW_OK) Log (LOG_ERR, "Cannot initialize GLEW");

	psx = new PSX (argv[1], (argc == 3) ? argv[2] : "");
	gui->Link (psx);
	psx->gpu->Link (GPUContext, psx);

	glfwWindowHint (GLFW_VISIBLE, GLFW_TRUE);
	glfwWindowHint (GLFW_MAXIMIZED, 1);
	//gui->GUIContext = glfwCreateWindow (VRAM_WIDTH, VRAM_HEIGHT, "PSX", NULL, GPUContext);
	gui->GUIContext = GPUContext;
	glfwMakeContextCurrent (gui->GUIContext);

	glfwSetInputMode (gui->GUIContext, GLFW_STICKY_KEYS, GLFW_TRUE);
    glEnable (GL_DEBUG_OUTPUT);
	glDisable (GL_DEPTH_TEST);
    glDebugMessageCallback (GLMessageCallback, nullptr);

	IMGUI_CHECKVERSION ();
	ImGui::CreateContext ();
	ImGui::StyleColorsClassic ();

	ImGui_ImplGlfw_InitForOpenGL (gui->GUIContext, true);
	ImGui_ImplOpenGL3_Init ("#version 330 core");

	//gui->InitGLAndImgui ();

	StartThread (InputLoop);
	StartThread (DebugLoop);

	glfwSetKeyCallback (gui->GUIContext, KeyCallback);
	glfwMakeContextCurrent (psx->gpu->GPUContext);

	while (!glfwWindowShouldClose (gui->GUIContext)) {
		psx->Clock ();
		if (psx->gpu->hitVBlankSignal || psx->freeze || psx->gpu->debugLock) gui->DrawScreen ();
	}
}

void InputLoop () {
	for (;;) {
		glfwPollEvents ();
		Sleep (50);
	}
}

void DebugLoop () {
	for (;;) {
		psx->clocksPreviousSecond = psx->clocksThisSecond;
		psx->clocksThisSecond = 0;
		Sleep (1000);
	}
}

void KeyCallback (GLFWwindow*, int key, int, int action, int) {
	if (action == GLFW_REPEAT) return;

	u8 keySet = action == GLFW_PRESS ? 0 : 1; // 0 is pressed
	u8 keyUpdate = 0;

	switch (key) {
		case GLFW_KEY_LEFT_SHIFT: keyUpdate = 0; break; // SELECT
		case GLFW_KEY_K: keyUpdate = 1; break; // L3
		case GLFW_KEY_L: keyUpdate = 2; break; // R3
		case GLFW_KEY_ENTER: keyUpdate = 3; break; // Start

		case GLFW_KEY_W: keyUpdate = 4; break; // Up
		case GLFW_KEY_D: keyUpdate = 5; break; // Right
		case GLFW_KEY_S: keyUpdate = 6; break; // Down
		case GLFW_KEY_A: keyUpdate = 7; break; // Left

		case GLFW_KEY_O: keyUpdate = 8; break; // L2
		case GLFW_KEY_P: keyUpdate = 9; break; // R2
		case GLFW_KEY_Q: keyUpdate = 10; break; // L1
		case GLFW_KEY_E: keyUpdate = 11; break; // R1

		case GLFW_KEY_Z: keyUpdate = 12; break; // Triangle
		case GLFW_KEY_C: keyUpdate = 13; break; // Circle
		case GLFW_KEY_X: keyUpdate = 14; break; // Cross
		case GLFW_KEY_V: keyUpdate = 15; break; // Square
		default: break;
	}

	SetBit (psx->ctrl->buttonsPressed, keyUpdate, keySet);
}
