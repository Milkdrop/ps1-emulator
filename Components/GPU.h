#ifndef GPU_H
#define GPU_H
#include "../utils.h"
#include "../PSX.h"
#include "CPU.h"

class GPU {
public:
	GPU () {}
	void Link (GLFWwindow* _GPUContext, PSX* _psx);
	u32 GPUREAD;
	u32 GPUSTAT;

	void GP0CMD (u32 value);
	void GP1CMD (u32 value);
	u32 GetGPUREAD ();
	void Render ();
	void Clear ();

	u8 PAL;
	void Tick ();
	u8 hitDotclockSignal;
	u8 isInHBlank, hitHBlankSignal;
	u8 isInVBlank, hitVBlankSignal;

	GLFWwindow *GPUContext;
	Vector2 displayOffset, displayXRange, displayYRange, displayResolution;

	// debug - these can be private
	u32 outputTexture, VRAMStencilTexture, VRAMCopyTexture;

	// debug - these can be deleted
	Vector2 debugCurrentlyDrawnTextureSize;
	u8 debugLock = 0;
	bool debug = 0;
	u64 renderTime = 0, loadTextureDataTime = 0;
	u8 lastSemiTransparencyMode = 0;
	u32 lastTexWindow = 0, lastTexPage = 0;

private:
	// Drawing settings
	Vector2 drawingOffset;
	Color renderColor;
	std::vector<Vertex> lineVertices;

	// Texture state
	u8 texturesCanBeDisabled;
	Vector2 texturedRectFlip;
	Vector2 textureWindowMask;
	Vector2 textureWindowOffset;
	u32 textureDataSize;

	// Copy Rect state
	Vector2 copyCoords;
	Vector2 copyBoundsX;
	i32 copyDataLeft;
	u8 ValueBuffer24Bit[4 * 3];
	u8 ValueBuffer24BitIndex;

	// General state
	void InstructionDone ();
	void Reset ();
	u8 instruction, readyForNewInstruction;
	u8 isDrawingPolyLine, lastType, drawingPolyLineOdd, lastPolyLineValue;
	Vector2 paramFIFO[GPU_MAX_PACKET_QUEUE];
	i32 paramFIFOSize;
	u8 disabled;
	Rect drawingArea;

	// Timing
	u32 tick;
	u16 dotOnLine;
	u16 lineOnScreen;
	
	// Texpage
	Vector2 texpageBase;
	u8 semiTransparencyMode, texpageColorDepth, textureDisable;

	// Components
	PSX* psx;

	// Instructions
	void FillRect ();
	void PushLineVertex (u32 value);
	void DrawLine (u8 type);
	void DrawRect (u8 type);
	void DrawPoly (u8 type);

	void SetTexPage (u32 value);
	void SetTextureWindow (u32 value);
	void SetDrawingAreaTopLeft (Vector2 v);
	void SetDrawingAreaBottomRight (Vector2 v);
	void SetDrawingOffset (u32 value);
	void SetMaskSetting (u32 value);
	void SetStartOfDisplayArea (u32 value);
	void SetHorizontalDisplayRange (u32 value);
	void SetVerticalDisplayRange (u32 value);

	// GP1 Instructions
	void SetDisplayMode (u32 value);
	u32 GetInfoSave[4];
	
	// Drawing Backend
	u8 rectIndices[6] = {
		0, 1, 2,
		1, 2, 3
	};

	u32 mainShader;
	u32 VAO, VBO, EBO, FBO, stencilBufferObject;
	u32 readPBO, stencilPBO;
	u32* VRAMBuffer;
	u32* VRAMBufferWrite;
	u32 textureData[VRAM_WIDTH * VRAM_HEIGHT];
	u8* stencilBuffer;

	void InitGLBackend ();
	void UpdateVRAMBuffer ();
	Color GetPixel (Vector2 point);
	Color GetPixel (u16 y, u16 x);

	void LoadTextureData (Vector2 texpageOffset, Vector2 textureSize, Vector2 CLUTLocation, Vector2 textureFlip = Vector2 (1, 1));
	void PushPixelToTextureData (Color pixel);

	// GL Backend
	void _GLDrawLine (std::vector<Vertex> &v, u8 semiTransparent = 0);
	void _GLFillRect (Vector2 position, Vector2 size, Color color);
	void _GLDrawPoly (const u8 vSize, Vertex* v, u8 textured = 0, u8 semiTransparent = 0, u8 clipToDrawingArea = 1);
	void _GLPutPixel (Vector2 position, Color color);
	void _GLUpdateLineVertices (u16 vSize, Vertex* v);
	void _GLUpdateVertices (u16 vSize, Vertex* v);
	void _GLUpdateVertices (std::vector<Vertex> &v);
	void _GLVRAMTransfer (Vector2 destination, Vector2 size);
	void _GLStartDrawToStencilBuffer ();
	void _GLEndDrawToStencilBuffer ();
	void _GLSetSemiTransparencyMode (u8 mode);
	void _GLUpdateDrawingArea (Rect rect);

	// Helpers
	Vector2 GetMaskedCopyCoords (Vector2 originalCoords);
	Vector2 GetMaskedCopySize (Vector2 originalSize);
	void IncrementCopyCoords ();
	Vector2 ParseCLUT (u16 value);
	Vector2 ParseTexpageBase (u16 value);
	Vector2 ParseTexpageOffset (u16 value);
	u8 ParseColorDepth (u16 value);
	void UpdateTexpageGPUSTAT ();
	void ClipCoordinates (const u8 vSize, Vertex* v);

	// Debug
	void DebugIncomingCommand (u32 value);
	void DebugCurrentCommand ();
};

#endif