#include "Controller.h"

void Controller::Link (PSX* _psx) {
	psx = _psx;
}

void Controller::TX (u32 value) {
	Log (LOG_DEBUG_CTRL, "CTRL TX: 0x%08x", value);
	if (GetBit (JOYCTRL, 13)) return;

	LastTX = value & 0xFF;

	//if (GetBit (JOYCTRL, 13) == 0 || GetBit (JOYCTRL, 1) == 0) {
		if (GetBit (JOYCTRL, 0) && GetBit (JOYCTRL, 12)) {
			psx->EnqueueIRQ (IRQ_CTRL, 200);
			lastINT = CTRL_TX_INT;
		} else Log (LOG_DEBUG_CTRL, "TX Sent but TXEN / ACKinterrupt is off");
	//} else Log (LOG_DEBUG_CTRL, "Ignored TX IRQ");
}

u8 Controller::RX () {
	Log (LOG_DEBUG_CTRL, "Controller RX: %02xh", LastTX);

	if (LastTX == 1) {
		RXFifo[0] = 0;
		RXFifo[1] = 0x41;
		RXFifo[2] = 0x5A;
		RXFifo[3] = buttonsPressed & 0xFF;
		RXFifo[4] = buttonsPressed >> 8;
		RXIndex = 0;
	}

	return PopRX ();
}

u8 Controller::PopRX () {
	u8 returnValue = RXFifo[RXIndex++];
	if (RXIndex >= 5) RXIndex = 5;
	return returnValue;
}

void Controller::SetJOYCTRL (u32 value) {
	JOYCTRL = value;
	Log (LOG_DEBUG_CTRL, "Set JOYCTRL: 0x%08x", value);
}

u32 Controller::GetJOYCTRL () {
	return JOYCTRL;
}

void Controller::IRQFired () {
	if (GetBit (JOYCTRL, 1)) {
		Log (LOG_DEBUG_CTRL, "JOYCTRL /JOYn output set");
	}

	SetBit (JOYSTAT, 7);
	SetBit (JOYSTAT, 9);
	SetBit (JOYSTAT, 1); // Ready to send stuff

	//if (GetBit (JOYCTRL, 13) == 0 || GetBit (JOYCTRL, 1) == 0) {
		if (CTRL_TX_INT) {
			Log (LOG_DEBUG_CTRL, "IRQ7 TX Ack");

			u8 RXINTBytes = 1 << GetBits (JOYCTRL, 8, 2);
			Log (LOG_DEBUG_CTRL, "RX Interrupt bytes: %d", RXINTBytes);
			if (GetBit (JOYCTRL, 11)) {
				lastINT = CTRL_RX_INT;
			}
		} else {
			Log (LOG_DEBUG_CTRL, "IRQ7 RX Ack");
		}
	//}
}