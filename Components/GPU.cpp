#include "GPU.h"

// TODO make VRAM Fills wrap-around
// TODO Non-rectangular textures ?

void GPU::Link (GLFWwindow* _GPUContext, PSX* _psx) {
	GPUContext = _GPUContext;
	psx = _psx;
	InitGLBackend ();
	Reset ();

	/*
	Vector2 rectPosition = Vector2 (-1, -5);
	Vector2 rectSize = Vector2 (20, 8);

	Vertex v[] = {
		rectPosition,
		rectPosition + Vector2 (rectSize.x, 0),
		rectPosition + Vector2 (0, rectSize.y),
		rectPosition + rectSize
	};

	for (u8 i = 0; i < 4; i++) {
		v[i].color = Color (0xFF0000FF);
	}

	_GLDrawPoly (4, v, 0, 0);

	debugLock = 1;
	*/
}

// VRAM doesn't really get updated during this VRAM -> CPU transfer, although it should... somehow
u32 GPU::GetGPUREAD () {
	if (copyDataLeft > 0) { // GPUREAD Already set by GP1
		if (GetBit (GPUSTAT, 21)) {
			// TODO - Maybe verify this implementation too
			GPUREAD = GetPixel (copyCoords).To24Bit ();
			IncrementCopyCoords ();
			copyDataLeft -= 3;
		} else {
			GPUREAD = GetPixel (copyCoords).To15Bit ();
			IncrementCopyCoords ();
			GPUREAD |= GetPixel (copyCoords).To15Bit () << 16;
			IncrementCopyCoords ();

			copyDataLeft -= 4;
		}
	}

	if (copyDataLeft <= 0) {
		SetBit (GPUSTAT, 27, 0);
	}

	return GPUREAD;
}

void GPU::GP0CMD (u32 value) {
	/*Vertex tempv[] = {
		Vector2 (0, 0),
		Vector2 (1, 0),
		Vector2 (0, 1),
		Vector2 (1, 1)
	};

	for (u8 i = 0; i < 4; i++) {
		tempv[i].color = Color (0xFFFFFFFF);
	}
	
	_GLDrawPoly (4, tempv, 0, 0);*/

	if (readyForNewInstruction) {
		instruction = value >> 24;
		renderColor = Color (value & 0xFFFFFF);

		SetBit (GPUSTAT, 26, 0); // Not ready to receive CMD Word
		readyForNewInstruction = 0;
		paramFIFOSize = 0;

		if (debug) DebugIncomingCommand (value);

		switch (instruction) {
			case 0x00: InstructionDone (); break; // NOP
			case 0x01: InstructionDone (); break; // Clear GPU Cache
			case 0x1F: SetBit (GPUSTAT, 24); break; // TODO - Generate IRQ1 as well
			case 0xE1: SetTexPage (value); break;
			case 0xE2: SetTextureWindow (value); break;
			case 0xE3: SetDrawingAreaTopLeft (Vector2 (GetBits (value, 0, 10), GetBits (value, 10, 9))); break;
			case 0xE4: SetDrawingAreaBottomRight (Vector2 (GetBits (value, 0, 10), GetBits (value, 10, 9))); break;
			case 0xE5: SetDrawingOffset (value); break;
			case 0xE6: SetMaskSetting (value); break;

			default: break;
		}
	} else {
		SetBit (GPUSTAT, 28, 0); // Not ready to receive DMA Block

		if (paramFIFOSize < GPU_MAX_PACKET_QUEUE) {
			paramFIFO[paramFIFOSize].y = (i16) (value >> 16);
			paramFIFO[paramFIFOSize].x = (i16) (value & 0xFFFF);

			paramFIFOSize++;
		} else {
			if (instruction < 0xA0 || instruction >= 0xC0) {
				Log (LOG_WARN, "GPU Command parameters are overflowing the FIFO. Instruction: %xh", instruction);
			}
		}

		if (GPU_PARAMFIFO_REQUIREMENT[instruction] == 0) {
			Log (LOG_WARN, "Instruction %xh not in GPU_PARAMFIFO_REQUIREMENT", instruction);
		}

		if (paramFIFOSize < GPU_PARAMFIFO_REQUIREMENT[instruction]) return;
		if (debug) DebugCurrentCommand ();

		if (instruction == 0x02) {
			FillRect (); 
		} else if (instruction >= 0x80 && instruction < 0xA0) {
			// VRAM -> VRAM
			UpdateVRAMBuffer ();
			Vector2 size = GetMaskedCopySize (paramFIFO[2]);
			//Log (LOG_DEBUG_GPU, "VRAM->VRAM Transfer: X%d:Y%d", size.x, size.y);

			textureDataSize = 0;
			for (i32 y = 0; y < size.y; y++) {
				for (i32 x = 0; x < size.x; x++) {
					textureData[textureDataSize++] = GetPixel (paramFIFO[0] + Vector2 (x, y)).To24Bit ();
				}
			}

			_GLVRAMTransfer (paramFIFO[1], size);
			InstructionDone ();

		} else if (instruction >= 0xA0 && instruction < 0xC0) {
			// CPU -> VRAM
			if (paramFIFOSize == 3) { // First initialization
				Vector2 rectSize = GetMaskedCopySize (paramFIFO[1]);
				copyDataLeft = (u32) rectSize.x * rectSize.y * 2;
				if (GetBit (GPUSTAT, 21)) { rectSize.x /= 3; rectSize.x *= 2; }

				//Log (LOG_DEBUG_GPU, "CPU->VRAM Establish Copy data: X%d:Y%d = %d", rectSize.x, rectSize.y, copyDataLeft);
				textureDataSize = 0;
			}

			if (GetBit (GPUSTAT, 21)) {
				((u32*) ValueBuffer24Bit)[ValueBuffer24BitIndex >> 2] = value;
				ValueBuffer24BitIndex += 4;

				if (ValueBuffer24BitIndex == 12 || copyDataLeft < 12) {
					for (u8 index = 0; index < 12; index += 3) {
						u32 outputValue = 0;
						for (u8 i = 0; i < 3; i++) {
							outputValue |= ValueBuffer24Bit[index + i] << (i << 3);
						}

						if (copyDataLeft > 0) {
							textureData[textureDataSize++] = Color (outputValue).To24Bit ();
							copyDataLeft -= 3;
						}
					}

					ValueBuffer24BitIndex = 0;
				}
			} else {
				textureData[textureDataSize++] = Color (value & 0xFFFF, COLOR_16BIT).To24Bit ();
				textureData[textureDataSize++] = Color (value >> 16, COLOR_16BIT).To24Bit ();
				copyDataLeft -= 4;
			}
			
			// If the transfer is interrupted before reaching the end, nothing will get drawn... maybe that's an issue
			if (copyDataLeft <= 0) {
				Vector2 copyCoords = GetMaskedCopyCoords (paramFIFO[0]);
				Vector2 size = GetMaskedCopySize (paramFIFO[1]);
				if (GetBit (GPUSTAT, 21)) { size.x /= 3; size.x *= 2; }

				_GLVRAMTransfer (copyCoords, size);
				InstructionDone ();
			}

		} else if (instruction >= 0xC0 && instruction < 0xE0) {
			// VRAM -> CPU
			UpdateVRAMBuffer ();
			copyCoords = GetMaskedCopyCoords (paramFIFO[0]);
			Vector2 rectSize = GetMaskedCopySize (paramFIFO[1]);
			copyDataLeft = (u32) rectSize.x * rectSize.y * 2;
			if (GetBit (GPUSTAT, 21)) { rectSize.x /= 3; rectSize.x *= 2; }

			//Log (LOG_DEBUG_GPU, "VRAM->CPU Establish Copy data: X%d:Y%d = %d", rectSize.x, rectSize.y, copyDataLeft);
			copyBoundsX = Vector2 (copyCoords.x, copyCoords.x + rectSize.x);

			SetBit (GPUSTAT, 27); // Send actual data using GPUREAD
			InstructionDone ();

		} else if (isDrawingPolyLine) {
			PushLineVertex (value);
		} else {
			u8 type = 0;

			type |= GetBit (instruction, 0)?RENDER_RAW_TEXTURE:0;
			type |= GetBit (instruction, 1)?RENDER_SEMITRANSPARENT:0;
			type |= GetBit (instruction, 2)?RENDER_TEXTURED:0;
			type |= GetBit (instruction, 3)?RENDER_QUAD_OR_POLY:0;
			type |= GetBit (instruction, 4)?RENDER_SHADED:0;

			if (textureDisable && texturesCanBeDisabled) {
				type &= ~RENDER_TEXTURED;
			}

			lastType = type;
			switch (GetBits (instruction, 5, 2)) {
				case 0: Log (LOG_WARN, "Unhandled GP0 instruction: %xh", instruction); break;
				case 1: DrawPoly (type); break;
				case 2: DrawLine (type); break;
				case 3: DrawRect (type); break;
			}
		}
	}
}

void GPU::FillRect () {
	Vector2 positionVector = Vector2 (paramFIFO[0].x & 0x3F0, paramFIFO[0].y & 0x1FF);
	Vector2 sizeVector = Vector2 (((paramFIFO[1].x & 0x3FF) + 0x0F) & (~0x0F), paramFIFO[1].y & 0x1FF);

	_GLFillRect (positionVector, sizeVector, renderColor);
	InstructionDone ();
}

void GPU::PushLineVertex (u32 value) {
	if (value == 0x55555555 || value == 0x50005000) {
		for (u32 i = 0; i < lineVertices.size (); i++) lineVertices[i].point += drawingOffset;
		_GLDrawLine (lineVertices, lastType & RENDER_SEMITRANSPARENT);
		isDrawingPolyLine = 0;
		InstructionDone ();
	} else {
		if (lastType & RENDER_SHADED) {
			if (drawingPolyLineOdd) {
				lineVertices.push_back (Vertex (Vector2 (value & 0xFFFF, value >> 16), lastPolyLineValue));
			}
		} else {
			lineVertices.push_back (Vertex (Vector2 (value & 0xFFFF, value >> 16), renderColor));
		}

		lastPolyLineValue = value;
		drawingPolyLineOdd = 1 - drawingPolyLineOdd;
	}
}

void GPU::DrawLine (u8 type) {
	lineVertices.clear ();

	lineVertices.push_back (Vertex (paramFIFO[0], renderColor));
	if (type & RENDER_SHADED) lineVertices.push_back (Vertex (paramFIFO[2], paramFIFO[1].To32 ()));
	else lineVertices.push_back (Vertex (paramFIFO[1], renderColor));

	if (!(type & RENDER_QUAD_OR_POLY)) {
		_GLDrawLine (lineVertices, type & RENDER_SEMITRANSPARENT);
		InstructionDone ();
	} else {
		isDrawingPolyLine = 1;
		drawingPolyLineOdd = 0;
	}
}

void GPU::DrawRect (u8 type) {
	Vector2 rectPosition = paramFIFO[0];
	Vector2 rectSize;
	
	if (GetBit (rectPosition.x, 10)) rectPosition.x = GetBits (rectPosition.x, 0, 10) - (1 << 10);
	if (GetBit (rectPosition.y, 10)) rectPosition.y = GetBits (rectPosition.y, 0, 10) - (1 << 10);

	// Rectangle size
	switch (GetBits (type, 3, 2)) {
		case 0: rectSize = paramFIFO[(type & RENDER_TEXTURED) ? 2 : 1]; break;
		case 1: rectSize = Vector2 (1, 1); break;
		case 2: rectSize = Vector2 (8, 8); break;
		case 3: rectSize = Vector2 (16, 16); break;
	}

	// No 1x1 textured nonsense
	if (rectSize == Vector2 (1, 1)) {
		type &= ~RENDER_TEXTURED;
	}

	Vertex v[] = {
		rectPosition,
		rectPosition + Vector2 (rectSize.x, 0),
		rectPosition + Vector2 (0, rectSize.y),
		rectPosition + rectSize
	};

	if (type & RENDER_TEXTURED) {
		Vector2 CLUTCoords = ParseCLUT (paramFIFO[1].y);
		Vector2 texpageOffset = ParseTexpageOffset (paramFIFO[1].x);

		switch (texpageColorDepth) {
			case TEXCOLORDEPTH_4BIT: texpageOffset.x /= 4; break;
			case TEXCOLORDEPTH_8BIT: texpageOffset.x /= 2; break;
			case TEXCOLORDEPTH_15BIT: break;
		}

		LoadTextureData (texpageOffset, rectSize, CLUTCoords, texturedRectFlip);

		v[0].texCoords = Vector2 (0, 0);
		v[1].texCoords = Vector2 (1, 0);
		v[2].texCoords = Vector2 (0, 1);
		v[3].texCoords = Vector2 (1, 1);
	}

	for (u8 i = 0; i < 4; i++) {
		v[i].color = Color (((type & RENDER_RAW_TEXTURE) && (type & RENDER_TEXTURED)) ? 0xFF808080 : renderColor);
		v[i].point += drawingOffset;
	}

	if (rectSize.x < 1024 && rectSize.y < 512) {
		_GLDrawPoly (4, v, (type & RENDER_TEXTURED) != 0, (type & RENDER_SEMITRANSPARENT) != 0);
	} else {
		Log (LOG_WARN, "Rect draw discarded: %d %d", rectSize.x, rectSize.y);
	}

	InstructionDone ();
}

void GPU::DrawPoly (u8 type) {
	Vertex v[4];

	if (type & RENDER_TEXTURED) {
		Vector2 CLUTCoords;
		Vector2 texpageCoords[4];

		if (type & RENDER_SHADED) {
			CLUTCoords = ParseCLUT (paramFIFO[1].y);
			texpageBase = ParseTexpageBase (paramFIFO[4].y);
			texpageCoords[0] = ParseTexpageOffset (paramFIFO[1].x);
			texpageCoords[1] = ParseTexpageOffset (paramFIFO[4].x);
			texpageCoords[2] = ParseTexpageOffset (paramFIFO[7].x);
			texpageCoords[3] = ParseTexpageOffset (paramFIFO[(type & RENDER_QUAD_OR_POLY)?10:7].x);

			semiTransparencyMode = GetBits (paramFIFO[4].y, 5, 2);
			texpageColorDepth = GetBits (paramFIFO[4].y, 7, 2);
			textureDisable = GetBit (paramFIFO[4].y, 11);

			v[0] = Vertex (paramFIFO[0], renderColor);
			v[1] = Vertex (paramFIFO[3], paramFIFO[2].To32 ());
			v[2] = Vertex (paramFIFO[6], paramFIFO[5].To32 ());
			if (type & RENDER_QUAD_OR_POLY) v[3] = Vertex (paramFIFO[9], paramFIFO[8].To32 ());
		} else {
			CLUTCoords = ParseCLUT (paramFIFO[1].y);
			texpageBase = ParseTexpageBase (paramFIFO[3].y);
			texpageCoords[0] = ParseTexpageOffset (paramFIFO[1].x);
			texpageCoords[1] = ParseTexpageOffset (paramFIFO[3].x);
			texpageCoords[2] = ParseTexpageOffset (paramFIFO[5].x);
			texpageCoords[3] = ParseTexpageOffset (paramFIFO[(type & RENDER_QUAD_OR_POLY)?7:5].x);

			semiTransparencyMode = GetBits (paramFIFO[3].y, 5, 2);
			texpageColorDepth = GetBits (paramFIFO[3].y, 7, 2);
			textureDisable = GetBit (paramFIFO[3].y, 11);

			v[0] = Vertex (paramFIFO[0], renderColor);
			v[1] = Vertex (paramFIFO[2], renderColor);
			v[2] = Vertex (paramFIFO[4], renderColor);
			if (type & RENDER_QUAD_OR_POLY) v[3] = Vertex (paramFIFO[6], renderColor);
		}
		
		UpdateTexpageGPUSTAT ();

		if (type & RENDER_RAW_TEXTURE) {
			for (u8 i = 0; i < 4; i++) {
				v[i].color = Color (0xFF808080);
			}
		}

		Rect textureBoundingBox = CalculateBoundingBox ((type & RENDER_QUAD_OR_POLY) ? 4 : 3, texpageCoords);
		Vector2 textureSize = textureBoundingBox.bottomRight - textureBoundingBox.topLeft;

		// Recalculate offset from texpage Base
		switch (texpageColorDepth) {
			case TEXCOLORDEPTH_4BIT: textureBoundingBox.topLeft.x /= 4; break;
			case TEXCOLORDEPTH_8BIT: textureBoundingBox.topLeft.x /= 2; break;
			case TEXCOLORDEPTH_15BIT: break;
		}

		LoadTextureData (textureBoundingBox.topLeft, textureSize, CLUTCoords);

		if (type & RENDER_QUAD_OR_POLY) {
			v[0].texCoords = Vector2 (0, 0);
			v[1].texCoords = Vector2 (1, 0);
			v[2].texCoords = Vector2 (0, 1);
			v[3].texCoords = Vector2 (1, 1);
		} else {
			Rect boundingBox = CalculateBoundingBox (3, v);
			Vector2 boundingBoxSize = boundingBox.bottomRight - boundingBox.topLeft;

			for (u8 i = 0; i < 3; i++) {
				v[i].texCoords = Vector2f (v[i].point - boundingBox.topLeft) / boundingBoxSize;
			}
		}
	} else {
		if (type & RENDER_SHADED) {
			v[0] = Vertex (paramFIFO[0], renderColor);
			v[1] = Vertex (paramFIFO[2], paramFIFO[1].To32 ());
			v[2] = Vertex (paramFIFO[4], paramFIFO[3].To32 ());
			if (type & RENDER_QUAD_OR_POLY) v[3] = Vertex (paramFIFO[6], paramFIFO[5].To32 ());
		} else {
			v[0] = Vertex (paramFIFO[0], renderColor);
			v[1] = Vertex (paramFIFO[1], renderColor);
			v[2] = Vertex (paramFIFO[2], renderColor);
			if (type & RENDER_QUAD_OR_POLY) v[3] = Vertex (paramFIFO[3], renderColor);
		}
	}

	// Signed coordinates
	for (u8 i = 0; i < 4; i++) {
		if (GetBit (v[i].point.x, 10)) v[i].point.x = GetBits (v[i].point.x, 0, 10) - (1 << 10);
		if (GetBit (v[i].point.y, 10)) v[i].point.y = GetBits (v[i].point.y, 0, 10) - (1 << 10);
		v[i].point += drawingOffset;
	}

	u8 vSize = (type & RENDER_QUAD_OR_POLY) ? 4 : 3;
	Rect boundingBox = CalculateBoundingBox (vSize, v);
	Vector2 boundingBoxSize = boundingBox.bottomRight - boundingBox.topLeft;

	// Maximum size
	if (boundingBoxSize.x < 1024 && boundingBoxSize.y < 512) {
		_GLDrawPoly (vSize, v, (type & RENDER_TEXTURED) != 0, (type & RENDER_SEMITRANSPARENT) != 0);
	} else {
		Log (LOG_WARN, "Polygon draw discarded: %d %d", boundingBoxSize.x, boundingBoxSize.y);
	}

	InstructionDone ();
}

void GPU::LoadTextureData (Vector2 texpageOffset, Vector2 textureSize, Vector2 CLUTLocation, Vector2 textureFlip) {
	if (debug) {
		Log (LOG_DEBUG_GPU, "Load texture data: X%d:Y%d + X%d:Y%d (TexWindowOffset: X%d:Y%d, TexWindowSize: X%d:Y%d) (CLUT Coords: X%d:Y%d, Color Depth: %d)",
			texpageBase.x + texpageOffset.x, texpageBase.y + texpageOffset.y,
			textureSize.x, textureSize.y,
			textureWindowOffset.x, textureWindowOffset.y, 256 - (textureWindowMask.x << 3), 256 - (textureWindowMask.y << 3),
			CLUTLocation.x, CLUTLocation.y,
			texpageColorDepth
		);
	}

	u64 before = glfwGetTime () * 1000000;

	glUniform2i (glGetUniformLocation (mainShader, "texpageBase"), texpageBase.x, texpageBase.y);
	glUniform2i (glGetUniformLocation (mainShader, "texpageOffset"), texpageOffset.x, texpageOffset.y);
	glUniform2i (glGetUniformLocation (mainShader, "textureSize"), textureSize.x, textureSize.y);
	glUniform2i (glGetUniformLocation (mainShader, "CLUTLocation"), CLUTLocation.x, CLUTLocation.y);
	glUniform2i (glGetUniformLocation (mainShader, "textureFlip"), textureFlip.x, textureFlip.y);
	glUniform1i (glGetUniformLocation (mainShader, "textureColorDepth"), texpageColorDepth);
	
	loadTextureDataTime = glfwGetTime () * 1000000 - before;
}

void GPU::PushPixelToTextureData (Color pixel) {
	textureData[textureDataSize++] = pixel.To24Bit ();

	if (pixel.R == 0 && pixel.G == 0 && pixel.B == 0 && pixel.semiTransparent == 0) {
		textureData[textureDataSize - 1] = 0x00000000;
	}
}

void GPU::UpdateVRAMBuffer () {
	glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, FBO);

	glBindBuffer (GL_PIXEL_PACK_BUFFER, readPBO);
	glUnmapBuffer (GL_PIXEL_PACK_BUFFER);
	glReadPixels (0, 0, VRAM_WIDTH, VRAM_HEIGHT, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV, 0);
	VRAMBuffer = (u32*) glMapBuffer (GL_PIXEL_PACK_BUFFER, GL_READ_ONLY);

	glBindBuffer (GL_PIXEL_PACK_BUFFER, stencilPBO);
	glUnmapBuffer (GL_PIXEL_PACK_BUFFER);
	glReadPixels (0, 0, VRAM_WIDTH, VRAM_HEIGHT, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, 0);
	stencilBuffer = (u8*) glMapBuffer (GL_PIXEL_PACK_BUFFER, GL_READ_ONLY);

	glActiveTexture (GL_TEXTURE0 + 2);
	glBindTexture (GL_TEXTURE_2D, VRAMStencilTexture);
	glTexImage2D (GL_TEXTURE_2D, 0, GL_RED, VRAM_WIDTH, VRAM_HEIGHT, 0, GL_RED, GL_UNSIGNED_BYTE, stencilBuffer);
	glActiveTexture (GL_TEXTURE0 + 0);
	glBindTexture (GL_TEXTURE_2D, outputTexture);

	glBindBuffer (GL_PIXEL_PACK_BUFFER, 0);
	glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, 0);
}

Color GPU::GetPixel (Vector2 point) {
	return GetPixel (point.x, point.y);
}

Color GPU::GetPixel (u16 x, u16 y) {
	x &= 0x3FF;
	y &= 0x1FF;

	Color colorToReturn = Color (VRAMBuffer[y * VRAM_WIDTH + x]);
	if (stencilBuffer[y * VRAM_WIDTH + x]) {
		colorToReturn.semiTransparent = 1;
	}

	return colorToReturn;
}

void GPU::Render () {
	SetBit (GPUSTAT, 24);
}

void GPU::Clear () {
	glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, FBO);
	glStencilMask (0xFF);
	glClear (GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glStencilMask (0);
	glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, 0);
}

void GPU::ClipCoordinates (const u8 vSize, Vertex* v) {
	u8 correct = 0;

	// Clip negative
	while (!correct) {
		u8 correctX = 0, correctY = 0;

		for (i32 i = 0; i < vSize; i++) {
			if (v[i].point.x >= 0) correctX = 1;
			if (v[i].point.y >= 0) correctY = 1;
		}

		for (i32 i = 0; i < vSize; i++) {
			if (!correctX) v[i].point.x += 1024;
			if (!correctY) v[i].point.y += 512;
		}

		if (correctX && correctY) correct = 1;
	}

	correct = 0;

	// Clip big
	while (!correct) {
		u8 correctX = 0, correctY = 0;

		for (i32 i = 0; i < vSize; i++) {
			if (v[i].point.x < 1024) correctX = 1;
			if (v[i].point.y < 512) correctY = 1;
		}

		for (i32 i = 0; i < vSize; i++) {
			if (!correctX) v[i].point.x -= 1024;
			if (!correctY) v[i].point.y -= 512;
		}

		if (correctX && correctY) correct = 1;
	}
}

void GPU::_GLFillRect (Vector2 position, Vector2 size, Color color) {
	glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, FBO);

	Vertex v[4] = {
		{position, color},
		{position + Vector2 (size.x, 0), color},
		{position + Vector2 (0, size.y), color},
		{position + size, color}
	};

	_GLUpdateVertices (4, v);
	// Also clear stencil buffer
	glStencilFunc (GL_ALWAYS, 0x00, 0xFF);
	glStencilMask (0xFF);
	glDrawElements (GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, 0);
	glStencilMask (0);

	glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, 0);
}

void GPU::_GLDrawLine (std::vector<Vertex> &v, u8 semiTransparent) {
	glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, FBO);
	_GLUpdateVertices (v);
	glEnable (GL_SCISSOR_TEST);

	if (semiTransparent) {
		_GLSetSemiTransparencyMode (semiTransparencyMode);
		glUniform1i (glGetUniformLocation (mainShader, "semiTransparent"), 1);
	}

	if (GetBit (GPUSTAT, 12)) {
		// Apply drawing mask
		glStencilFunc (GL_EQUAL, 0, 0xFF);
		glDrawArrays (GL_LINE_STRIP, 0, v.size ());
	} else {
		// Clear stencil buffer if we overwrite everything
		glStencilFunc (GL_ALWAYS, 0x00, 0xFF);
		glStencilMask (0xFF);
		glDrawArrays (GL_LINE_STRIP, 0, v.size ());
		glStencilMask (0);
	}

	glStencilFunc (GL_ALWAYS, 0xFF, 0xFF);

	_GLStartDrawToStencilBuffer ();
	glDrawArrays (GL_LINE_STRIP, 0, v.size ());
	_GLEndDrawToStencilBuffer ();

	if (semiTransparent) {
		_GLSetSemiTransparencyMode (0);
		glUniform1i (glGetUniformLocation (mainShader, "semiTransparent"), 0);
	}

	glDisable (GL_SCISSOR_TEST);
	glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, 0);
}

void GPU::_GLDrawPoly (const u8 vSize, Vertex* v, u8 textured, u8 semiTransparent, u8 clipToDrawingArea) {
	ClipCoordinates (vSize, v);

	glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, FBO);
	if (clipToDrawingArea) glEnable (GL_SCISSOR_TEST);
	if (textured) glUniform1i (glGetUniformLocation (mainShader, "textured"), 1);
	
	if (semiTransparent) {
		_GLSetSemiTransparencyMode (semiTransparencyMode);
		glUniform1i (glGetUniformLocation (mainShader, "semiTransparent"), 1);
	}

	_GLUpdateVertices (vSize, v);

	if (GetBit (GPUSTAT, 12)) {
		// Apply drawing mask
		glStencilFunc (GL_EQUAL, 0, 0xFF);
		glDrawElements (GL_TRIANGLES, (vSize == 4) ? 6 : 3, GL_UNSIGNED_BYTE, 0);
	} else {
		// Clear stencil buffer if we overwrite everything
		glStencilFunc (GL_ALWAYS, 0x00, 0xFF);
		glStencilMask (0xFF);
		glDrawElements (GL_TRIANGLES, (vSize == 4) ? 6 : 3, GL_UNSIGNED_BYTE, 0);

		glStencilMask (0);
	}

	glStencilFunc (GL_ALWAYS, 0xFF, 0xFF);

	_GLStartDrawToStencilBuffer ();
	glDrawElements (GL_TRIANGLES, (vSize == 4) ? 6 : 3, GL_UNSIGNED_BYTE, 0);
	_GLEndDrawToStencilBuffer ();

	if (clipToDrawingArea) glDisable (GL_SCISSOR_TEST);
	if (textured) glUniform1i (glGetUniformLocation (mainShader, "textured"), 0);

	if (semiTransparent) {
		_GLSetSemiTransparencyMode (0);
		glUniform1i (glGetUniformLocation (mainShader, "semiTransparent"), 0);
	}

	glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, 0);
}

void GPU::_GLVRAMTransfer (Vector2 destination, Vector2 size) {
	UpdateVRAMBuffer ();

	glActiveTexture (GL_TEXTURE0 + 1);
	glBindTexture (GL_TEXTURE_2D, VRAMCopyTexture);
	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, size.x, size.y, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV, textureData);
	glActiveTexture (GL_TEXTURE0 + 0);
	glBindTexture (GL_TEXTURE_2D, outputTexture);

	Vertex v[4] = {
		Vertex (destination, Color (), Vector2f (0, 0)),
		Vertex (destination + Vector2 (size.x, 0), Color (), Vector2f (1, 0)),
		Vertex (destination + Vector2 (0, size.y), Color (), Vector2f (0, 1)),
		Vertex (destination + size, Color (), Vector2f (1, 1))
	};

	glUniform1i (glGetUniformLocation (mainShader, "drawVRAMCopyTexture"), 1);
	_GLDrawPoly (4, v, 1, 0, 0);
	glUniform1i (glGetUniformLocation (mainShader, "drawVRAMCopyTexture"), 0);

	UpdateVRAMBuffer ();
}

void GPU::_GLStartDrawToStencilBuffer () {
	glColorMask (GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	glUniform1i (glGetUniformLocation (mainShader, "renderToMask"), 1);
	glStencilMask (0xFF);
}

void GPU::_GLEndDrawToStencilBuffer () {
	glColorMask (GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glUniform1i (glGetUniformLocation (mainShader, "renderToMask"), 0);
	glStencilMask (0);
}

void GPU::_GLSetSemiTransparencyMode (u8 mode) {
	switch (mode) {
		case 0: glBlendEquation (GL_FUNC_ADD); glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); break; // B/2 + F/2
		case 1: glBlendEquation (GL_FUNC_ADD); glBlendFunc (GL_ONE, GL_ONE); break; // B+F
		case 2: glBlendEquation (GL_FUNC_REVERSE_SUBTRACT); glBlendFunc (GL_ONE, GL_ONE); break; // B-F
		case 3: glBlendEquation (GL_FUNC_ADD); glBlendFunc (GL_SRC_ALPHA, GL_ONE); break; // B+F/4
		default: break;
	}

	glUniform1i (glGetUniformLocation (mainShader, "semiTransparencyMode"), mode);
}

// Fast
void GPU::_GLUpdateVertices (u16 vSize, Vertex* v) {
	float vertices[7 * 6];

	for (int i = 0; i < vSize; i++) {
		vertices [i * 7] = v[i].point.x;
		vertices [i * 7 + 1] = v[i].point.y;

		vertices [i * 7 + 2] = v[i].texCoords.x;
		vertices [i * 7 + 3] = v[i].texCoords.y;

		vertices [i * 7 + 4] = v[i].color.R;
		vertices [i * 7 + 5] = v[i].color.G;
		vertices [i * 7 + 6] = v[i].color.B;
	}

	glBufferData (GL_ARRAY_BUFFER, sizeof (vertices), vertices, GL_DYNAMIC_DRAW);
}

// Slower variant (?), with constant reallocation
void GPU::_GLUpdateVertices (std::vector<Vertex> &v) {
	float* vertices = new float[v.size () * 7];

	for (u32 i = 0; i < v.size (); i++) {
		vertices [i * 7] = v[i].point.x;
		vertices [i * 7 + 1] = v[i].point.y;

		vertices [i * 7 + 2] = v[i].texCoords.x;
		vertices [i * 7 + 3] = v[i].texCoords.y;

		vertices [i * 7 + 4] = v[i].color.R;
		vertices [i * 7 + 5] = v[i].color.G;
		vertices [i * 7 + 6] = v[i].color.B;
	}

	glBufferData (GL_ARRAY_BUFFER, v.size () * 7 * sizeof (float), vertices, GL_DYNAMIC_DRAW);
	delete vertices;
}

void GPU::InitGLBackend () {
	if (GPUContext == NULL) Log (LOG_ERR, "GPU Context is null");

	glClearColor (0, 0, 0, 0);

	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable (GL_STENCIL_TEST);
	glStencilOp (GL_KEEP, GL_KEEP, GL_REPLACE);
	glStencilFunc (GL_ALWAYS, 0xFF, 0xFF);
	glStencilMask (0);
	
	glGenVertexArrays (1, &VAO);
	glBindVertexArray (VAO);

	glGenBuffers (1, &VBO);  
	glBindBuffer (GL_ARRAY_BUFFER, VBO);

	glGenBuffers (1, &readPBO);
	glBindBuffer (GL_PIXEL_PACK_BUFFER, readPBO);
	glBufferData (GL_PIXEL_PACK_BUFFER, VRAM_WIDTH * VRAM_HEIGHT * 4, NULL, GL_DYNAMIC_READ);
	glGenBuffers (1, &stencilPBO);
	glBindBuffer (GL_PIXEL_PACK_BUFFER, stencilPBO);
	glBufferData (GL_PIXEL_PACK_BUFFER, VRAM_WIDTH * VRAM_HEIGHT * 4, NULL, GL_DYNAMIC_READ);
	glBindBuffer (GL_PIXEL_PACK_BUFFER, 0);

	glGenBuffers (1, &EBO);
	glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData (GL_ELEMENT_ARRAY_BUFFER, sizeof (rectIndices), rectIndices, GL_STATIC_DRAW);

	glEnableVertexAttribArray (0);
	glEnableVertexAttribArray (1);
	glEnableVertexAttribArray (2);
	glVertexAttribPointer (0, 2, GL_FLOAT, GL_FALSE, 7 * sizeof (float), (void*) 0);
	glVertexAttribPointer (1, 2, GL_FLOAT, GL_FALSE, 7 * sizeof (float), (void*) (2 * sizeof(float)));
	glVertexAttribPointer (2, 3, GL_FLOAT, GL_FALSE, 7 * sizeof (float), (void*) (4 * sizeof(float)));

	glGenTextures (1, &outputTexture);
	glBindTexture (GL_TEXTURE_2D, outputTexture);
	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, VRAM_WIDTH, VRAM_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glGenFramebuffersEXT (1, &FBO);
	glGenRenderbuffersEXT (1, &stencilBufferObject);
	glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, FBO);

	glFramebufferTexture2DEXT (GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, outputTexture, 0);
	glBindRenderbufferEXT (GL_RENDERBUFFER_EXT, stencilBufferObject);
	glRenderbufferStorageEXT (GL_RENDERBUFFER_EXT, GL_STENCIL_INDEX, VRAM_WIDTH, VRAM_HEIGHT);
	glFramebufferRenderbufferEXT (GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, stencilBufferObject);

	if (glCheckFramebufferStatus (GL_FRAMEBUFFER_EXT) != GL_FRAMEBUFFER_COMPLETE) {
		Log (LOG_ERR, "Can't create framebuffer?");
	}

	glGenTextures (1, &VRAMCopyTexture);
	glBindTexture (GL_TEXTURE_2D, VRAMCopyTexture);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glGenTextures (1, &VRAMStencilTexture);
	glBindTexture (GL_TEXTURE_2D, VRAMStencilTexture);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glActiveTexture (GL_TEXTURE0 + 0);
	glBindTexture (GL_TEXTURE_2D, outputTexture);

	mainShader = _GLCreateShaderProgram ("Shaders/gpuShader.vert", "Shaders/gpuShader.frag");

	glUseProgram (mainShader);
	glUniform1i (glGetUniformLocation (mainShader, "VRAM"), 0);
	glUniform1i (glGetUniformLocation (mainShader, "VRAMCopyTexture"), 1);
	glUniform1i (glGetUniformLocation (mainShader, "VRAMStencilTexture"), 2);
	glUniform1i (glGetUniformLocation (mainShader, "renderToMask"), 0);
	glUniform1i (glGetUniformLocation (mainShader, "drawVRAMCopyTexture"), 0);
	glUniform2i (glGetUniformLocation (mainShader, "VRAMSize"), VRAM_WIDTH, VRAM_HEIGHT);

	glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, 0);
}

void GPU::GP1CMD (u32 value) {
	u8 opcode = value >> 24;
	opcode &= 0x3F;

	if (opcode >= 0x10 && opcode < 0x20) {
		u8 infoRequested = value & 0xF;
		copyDataLeft = 0; // Cancel copy rect GPUREAD transfer

		if (infoRequested >= 2 && infoRequested <= 8) {
			switch (infoRequested) { // New 208pin GPU implementation
				case 2: GPUREAD = GetInfoSave[0]; break;
				case 3: GPUREAD = GetInfoSave[1]; break;
				case 4: GPUREAD = GetInfoSave[2]; break;
				case 5: GPUREAD = GetInfoSave[3]; break;
				case 6: break;
				case 7: GPUREAD = 2; break;
				case 8: GPUREAD = 0; break;
			}
		}
	} else switch (opcode) {
		case 0x00: Reset (); break;
		case 0x01: InstructionDone (); break;
		case 0x02: SetBit (GPUSTAT, 24, 0); break;
		case 0x03: disabled = value & 1; if (disabled) Clear (); break;
		case 0x04: SetBit (GPUSTAT, 29, value & 0b1); SetBit (GPUSTAT, 30, value & 0b10); break; // TODO - GPU DMA Direction
		case 0x05: SetStartOfDisplayArea (value); break;
		case 0x06: SetHorizontalDisplayRange (value); break;
		case 0x07: SetVerticalDisplayRange (value); break;
		case 0x08: SetDisplayMode (value); break; // TODO
		case 0x09: texturesCanBeDisabled = value & 1; break;
		default: Log (LOG_WARN, "Unknown GP1 Command: 0x%02x", opcode);
	}
}

void GPU::SetDisplayMode (u32 value) {
	if (GetBit (value, 6)) displayResolution.x = 368;
	else switch (GetBits (value, 0, 2)) {
		case 0: displayResolution.x = 256; break;
		case 1: displayResolution.x = 320; break;
		case 2: displayResolution.x = 512; break;
		case 3: displayResolution.x = 640; break;
	}

	if (GetBit (value, 5) && GetBit (value, 2)) displayResolution.y = 480;
	else displayResolution.y = 240; 

	if (debug) {
		Log (LOG_DEBUG_GPU, "Set Display mode: %xh", value);
		Log (LOG_DEBUG_GPU, "Display area color depth is: %dBit", GetBit (value, 4) ? 24 : 16);
		Log (LOG_DEBUG_GPU, "Video mode is: %s", GetBit (value, 3) ? "PAL" : "NTSC");
		Log (LOG_DEBUG_GPU, "Resolution: X%d:Y%d", displayResolution.x, displayResolution.y);
	}

	if (GetBit (value, 5)) if (debug) Log (LOG_DEBUG_GPU, "GPU Vertical interlace has been set");
	if (GetBit (value, 7)) Log (LOG_WARN, "GPU Reverseflag has been set");

	GPUSTAT &= ~(0b111111 << 17);
	GPUSTAT |= GetBits (value, 0, 6) << 17;
	PAL = GetBit (GPUSTAT, 20);

	SetBit (GPUSTAT, 16, GetBit (value, 6));
	SetBit (GPUSTAT, 14, GetBit (value, 7));

	// TODO - Maybe implement vertical interlacing sometime
	SetBit (GPUSTAT, 19, 0);
	SetBit (GPUSTAT, 22, 0); 
}

void GPU::SetTexPage (u32 value) {
	GPUSTAT &= ~0x7FF;
	GPUSTAT |= value & 0x7FF;
	SetBit (GPUSTAT, 15, GetBit (value, 11));
	texturedRectFlip = Vector2 (GetBit (value, 12)?-1:1, GetBit (value, 13)?-1:1);

	texpageBase = ParseTexpageBase (GPUSTAT);
	semiTransparencyMode = GetBits (GPUSTAT, 5, 2);
	texpageColorDepth = GetBits (GPUSTAT, 7, 2);
	textureDisable = GetBit (GPUSTAT, 15);

	if (lastSemiTransparencyMode != semiTransparencyMode) {
		if (debug) Log (LOG_DEBUG_GPU, "GPU Semi transparency mode: %d", semiTransparencyMode);
		lastSemiTransparencyMode = semiTransparencyMode;
	}

	InstructionDone ();
}

void GPU::UpdateTexpageGPUSTAT () {
	GPUSTAT &= ~0x1FF;
	GPUSTAT |= (texpageBase.x >> 6) & 0xF;
	SetBit (GPUSTAT, 4, texpageBase.y == 256);
	GPUSTAT |= semiTransparencyMode << 5;
	GPUSTAT |= texpageColorDepth << 7;
	SetBit (GPUSTAT, 15, textureDisable);
}

void GPU::SetTextureWindow (u32 value) {
	GetInfoSave[0] = value & 0xFFFFFF;
	textureWindowMask = Vector2 (GetBits (value, 0, 5), GetBits (value, 5, 5));
	textureWindowOffset = Vector2 (GetBits (value, 10, 5), GetBits (value, 15, 5));
	glUniform2i (glGetUniformLocation (mainShader, "textureWindowOffset"), textureWindowOffset.x << 3, textureWindowOffset.y << 3);
	glUniform2i (glGetUniformLocation (mainShader, "textureWindowSize"), 256 - (textureWindowMask.x << 3), 256 - (textureWindowMask.y << 3));
	InstructionDone ();
}

// TODO - Drawing area is assumed max 1024x512, though our GPU is supposed to be a "New" one
void GPU::SetDrawingAreaTopLeft (Vector2 v) {
	GetInfoSave[1] = v.x | (v.y << 10);
	drawingArea.topLeft = v;
	_GLUpdateDrawingArea (drawingArea);
	InstructionDone ();
}

void GPU::SetDrawingAreaBottomRight (Vector2 v) {
	GetInfoSave[2] = v.x | (v.y << 10);
	drawingArea.bottomRight = v;
	_GLUpdateDrawingArea (drawingArea);
	InstructionDone ();
}

void GPU::_GLUpdateDrawingArea (Rect rect) {
	if (rect.topLeft.x < rect.bottomRight.x && rect.topLeft.y < rect.bottomRight.y) {
		GLsizei rectWidth = rect.bottomRight.x - rect.topLeft.x + 1;
		GLsizei rectHeight = rect.bottomRight.y - rect.topLeft.y + 1;
		if (debug) Log (LOG_DEBUG_GPU, "Updating drawing area: X%d:Y%d - X%d:Y%d", drawingArea.topLeft.x, drawingArea.topLeft.y, drawingArea.bottomRight.x, drawingArea.bottomRight.y);

		glScissor (rect.topLeft.x, rect.topLeft.y, rectWidth, rectHeight);
	}
}

void GPU::SetDrawingOffset (u32 value) {
	GetInfoSave[3] = value & 0xFFFFFF;
	drawingOffset = Vector2 (GetBits (value, 0, 10), GetBits (value, 11, 10));
	if (GetBit (value, 10)) drawingOffset.x = -drawingOffset.x;
	if (GetBit (value, 21)) drawingOffset.y = -drawingOffset.y;
	InstructionDone ();
}

void GPU::SetMaskSetting (u32 value) {
	SetBit (GPUSTAT, 11, GetBit (value, 0));
	SetBit (GPUSTAT, 12, GetBit (value, 1));

	glUniform1i (glGetUniformLocation (mainShader, "forceBit15"), GetBit (GPUSTAT, 11));
	InstructionDone ();
}

void GPU::SetStartOfDisplayArea (u32 value) {
	displayOffset = Vector2 (GetBits (value, 0, 10), GetBits (value, 10, 9));
	if (debug) Log (LOG_DEBUG_GPU, "Set Start of display area in VRAM: %xh: X%d:Y%d", value, displayOffset.x, displayOffset.y);
}

void GPU::SetHorizontalDisplayRange (u32 value) {
	displayXRange = Vector2 (GetBits (value, 0, 12) >> 3, GetBits (value, 12, 12) >> 3);
	if (debug) Log (LOG_DEBUG_GPU, "Horizontal Display Range: %d - %d", displayXRange.x, displayXRange.y);
}

void GPU::SetVerticalDisplayRange (u32 value) {
	displayYRange = Vector2 (GetBits (value, 0, 10), GetBits (value, 10, 10));
	if (debug) Log (LOG_DEBUG_GPU, "Vertical Display Range: %d - %d", displayYRange.x, displayYRange.y);
}

Vector2 GPU::GetMaskedCopyCoords (Vector2 originalCoords) {
	return Vector2 (originalCoords.x & 0x3FF, originalCoords.y & 0x1FF);
}

Vector2 GPU::GetMaskedCopySize (Vector2 originalSize) {
	return Vector2 (((originalSize.x - 1) & 0x3FF) + 1, ((originalSize.y - 1) & 0x1FF) + 1);
}

void GPU::IncrementCopyCoords () {
	copyCoords.x++;

	if (copyCoords.x >= copyBoundsX.y) {
		copyCoords.x = copyBoundsX.x;
		copyCoords.y++;
	}
}

Vector2 GPU::ParseCLUT (u16 value) {
	return Vector2 (GetBits (value, 0, 6) << 4, GetBits (value, 6, 9));
}

Vector2 GPU::ParseTexpageBase (u16 value) {
	return Vector2 (GetBits (value, 0, 4) << 6, GetBit (value, 4) << 8);
}

Vector2 GPU::ParseTexpageOffset (u16 value) {
	return Vector2 (GetBits (value, 0, 8), GetBits (value, 8, 8));
}

u8 GPU::ParseColorDepth (u16 value) {
	return GetBits (value, 7, 2);
}

void GPU::Reset () {
	GPUREAD = 0;
	GPUSTAT = 0x14802000;
	disabled = 1;
	texturesCanBeDisabled = 0;
	PAL = GetBit (GPUSTAT, 20);

	tick = 0;
	dotOnLine = 0;
	lineOnScreen = 0;

	memset (ValueBuffer24Bit, 0, sizeof (ValueBuffer24Bit));
	ValueBuffer24BitIndex = 0;

	paramFIFOSize = 0;
	copyDataLeft = 0;
	isDrawingPolyLine = 0;
	drawingOffset = Vector2 (0, 0);
	texturedRectFlip = Vector2 (0, 0);
	SetTexPage (0);
	SetTextureWindow (0);
	SetDisplayMode (0);
	SetDrawingAreaTopLeft (Vector2 (0, 0));
	SetDrawingAreaBottomRight (Vector2 (VRAM_WIDTH - 1, VRAM_HEIGHT - 1));
	InstructionDone ();

	// Maybe remove these?
	glUniform1i (glGetUniformLocation (mainShader, "semiTransparencyMode"), 0);
	glBlendEquation (GL_FUNC_ADD);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	Clear ();
}

void GPU::InstructionDone () {
	if (copyDataLeft > 0 && !GetBit (GPUSTAT, 27)) {
		Log (LOG_WARN, "Instruction done during an ongoing CPU VRAM transfer: %d bytes left", copyDataLeft);
		Log (LOG_DEBUG_GPU, "Instruction done during an ongoing CPU VRAM transfer: %d bytes left", copyDataLeft);
	}

	SetBit (GPUSTAT, 26);
	SetBit (GPUSTAT, 28);
	readyForNewInstruction = 1;
}

void GPU::Tick () {
	tick++;

	// This ticking is overall accurate but it's not evenly distributed
	u8 positionInTickChunk = 0;

	switch (displayResolution.x) {
		case 256: positionInTickChunk = tick % (7 * 10); break;
		case 320: positionInTickChunk = tick % (7 * 8); break;
		case 368: positionInTickChunk = tick % (7 * 7); break;
		case 512: positionInTickChunk = tick % (7 * 5); break;
		case 640: positionInTickChunk = tick % (7 * 4); break;

		default: Log (LOG_WARN, "GPU ticking detected an unknown resolution: %d", displayResolution.x);
	}

	if (positionInTickChunk < 11) hitDotclockSignal = 1;
	else hitDotclockSignal = 0;

	hitHBlankSignal = 0;
	hitVBlankSignal = 0;

	// Calculate H-Blank stuff by using the 256px numbers because I'm lazy ?
	if (tick % (7 * 10) < 11) {
		dotOnLine++;

		// TODO: Take into account games that use slightly smaller / larger display widths than the current resolution? 
		if (dotOnLine >= 256) {
			if (dotOnLine == 256) hitHBlankSignal = 1;

			isInHBlank = 1;

			if ((PAL && dotOnLine >= 340) || (!PAL && dotOnLine >= 341)) {
				isInHBlank = 0;
				dotOnLine = 0;
				lineOnScreen++;
				
				// Apparently I shouldn't worry much about vertical interlacing?
				if (lineOnScreen >= 240) {
					if (lineOnScreen == 240) {
						hitVBlankSignal = 1;
						Render ();
						psx->cpu->IRQ (IRQ_VBLANK);
					}

					isInVBlank = 1;

					if ((PAL && lineOnScreen >= 314) || (!PAL && lineOnScreen >= 263)) {
						isInVBlank = 0;
						lineOnScreen = 0;
						tick = 0;
					}
				}
			}
		}
	}
}

void GPU::DebugIncomingCommand (u32 value) {
	u8 localInstruction = value >> 24;

	switch (localInstruction) {
		case 0x00: break; // NOP
		case 0x01: break; // Clear cache
		case 0xE1: if (lastTexPage != value) { Log (LOG_DEBUG_GPU, "Set TexPage: 0x%08x", value); lastTexPage = value; } break;
		case 0xE2: if (lastTexWindow != value) { Log (LOG_DEBUG_GPU, "Set TexWindow: 0x%08x", value); lastTexWindow = value; } break;
		case 0xE3: Log (LOG_DEBUG_GPU, "Set DrawingArea Top-Left: 0x%08x", value); break;
		case 0xE4: Log (LOG_DEBUG_GPU, "Set DrawingArea Bottom-Right: 0x%08x", value); break;
		case 0xE5: Log (LOG_DEBUG_GPU, "Set Drawing Offset: 0x%08x", value); break;
		case 0xE6: Log (LOG_DEBUG_GPU, "Set Mask Bit Setting: 0x%08x", value); break;

		default: break;
	}
}

void GPU::DebugCurrentCommand () {
	if (instruction == 0x02) {
		Vector2 positionVector = Vector2 (paramFIFO[0].x & 0x3F0, paramFIFO[0].y & 0x1FF);
		Vector2 sizeVector = Vector2 (((paramFIFO[1].x & 0x3FF) + 0x0F) & (~0x0F), paramFIFO[1].y & 0x1FF);

		Log (LOG_DEBUG_GPU, "Fill rect: X%d:Y%d (size: X%d:Y%d)",
			positionVector.x, positionVector.y,
			sizeVector.x, sizeVector.y
		);

	} else if (instruction >= 0x80 && instruction < 0xA0) {
		Vector2 size = GetMaskedCopySize (paramFIFO[2]);

		Log (LOG_DEBUG_GPU, "Copy rect VRAM->VRAM: X%d:Y%d -> X%d:Y%d (size: X%d:Y%d)",
			paramFIFO[0].x, paramFIFO[0].y,
			paramFIFO[1].x, paramFIFO[1].y,
			size.x, size.y
		);

	} else if (instruction >= 0xA0 && instruction < 0xC0) {
		if (paramFIFOSize == 3) { // First initialization
			Vector2 tempCopyCoords = GetMaskedCopyCoords (paramFIFO[0]);
			Vector2 rectSize = GetMaskedCopySize (paramFIFO[1]);

			Log (LOG_DEBUG_GPU, "Copy rect CPU->VRAM: X%d:Y%d (size: X%d:Y%d)",
				tempCopyCoords.x, tempCopyCoords.y,
				rectSize.x, rectSize.y
			);
		} else { // Guard this from not printing that the transfer has finished instantly
			if (copyDataLeft <= 0) {
				Log (LOG_DEBUG_GPU, "Copy rect CPU->VRAM finished");
			}
		}

	} else if (instruction >= 0xC0 && instruction < 0xE0) {
		Vector2 tempCopyCoords = GetMaskedCopyCoords (paramFIFO[0]);
		Vector2 rectSize = GetMaskedCopySize (paramFIFO[1]);

		Log (LOG_DEBUG_GPU, "Copy rect VRAM->CPU: X%d:Y%d (size: X%d:Y%d)",
			tempCopyCoords.x, tempCopyCoords.y,
			rectSize.x, rectSize.y
		);

	} else {
		u8 type = 0;

		type |= GetBit (instruction, 0)?RENDER_RAW_TEXTURE:0;
		type |= GetBit (instruction, 1)?RENDER_SEMITRANSPARENT:0;
		type |= GetBit (instruction, 2)?RENDER_TEXTURED:0;
		type |= GetBit (instruction, 3)?RENDER_QUAD_OR_POLY:0;
		type |= GetBit (instruction, 4)?RENDER_SHADED:0;

		if (textureDisable && texturesCanBeDisabled) {
			type &= ~RENDER_TEXTURED;
		}

		switch (GetBits (instruction, 5, 2)) {
			case 0: Log (LOG_WARN, "Unhandled GP0 instruction: %xh", instruction); break;
			case 1: {
				Vertex v[4];

				if (type & RENDER_TEXTURED) {
					//Vector2 CLUTCoords;
					//Vector2 texpageBase;
					//Vector2 texpageCoords[4];
					//u8 textureColorDepth;

					if (type & RENDER_SHADED) {
						/*CLUTCoords = ParseCLUT (paramFIFO[1].y);
						texpageBase = ParseTexpageBase (paramFIFO[4].y);
						texpageCoords[0] = ParseTexpageOffset (paramFIFO[1].x);
						texpageCoords[1] = ParseTexpageOffset (paramFIFO[4].x);
						texpageCoords[2] = ParseTexpageOffset (paramFIFO[7].x);
						texpageCoords[3] = ParseTexpageOffset (paramFIFO[(type & RENDER_QUAD_OR_POLY)?10:7].x);
						textureColorDepth = GetBits (paramFIFO[4].y, 7, 2);*/

						v[0] = Vertex (paramFIFO[0], renderColor);
						v[1] = Vertex (paramFIFO[3], paramFIFO[2].To32 ());
						v[2] = Vertex (paramFIFO[6], paramFIFO[5].To32 ());
						for (u8 i = 0; i < 3; i++) v[i].point += drawingOffset;

						if (type & RENDER_QUAD_OR_POLY) {
							v[3] = Vertex (paramFIFO[9] + drawingOffset, paramFIFO[8].To32 ());
							Log (LOG_DEBUG_GPU, "Draw poly: X%d:Y%d (0x%08x) - X%d:Y%d (0x%08x) - X%d:Y%d (0x%08x) - X%d:Y%d (0x%08x) / TEXTURED",
								v[0].point.x, v[0].point.y, v[0].color.To24Bit (),
								v[1].point.x, v[1].point.y, v[1].color.To24Bit (),
								v[2].point.x, v[2].point.y, v[2].color.To24Bit (),
								v[3].point.x, v[3].point.y, v[3].color.To24Bit ()
							);
						} else {
							Log (LOG_DEBUG_GPU, "Draw poly: X%d:Y%d (0x%08x) - X%d:Y%d (0x%08x) - X%d:Y%d (0x%08x) / TEXTURED",
								v[0].point.x, v[0].point.y, v[0].color.To24Bit (),
								v[1].point.x, v[1].point.y, v[1].color.To24Bit (),
								v[2].point.x, v[2].point.y, v[2].color.To24Bit ()
							);
						}
					} else {
						/*CLUTCoords = ParseCLUT (paramFIFO[1].y);
						texpageBase = ParseTexpageBase (paramFIFO[3].y);
						texpageCoords[0] = ParseTexpageOffset (paramFIFO[1].x);
						texpageCoords[1] = ParseTexpageOffset (paramFIFO[3].x);
						texpageCoords[2] = ParseTexpageOffset (paramFIFO[5].x);
						texpageCoords[3] = ParseTexpageOffset (paramFIFO[(type & RENDER_QUAD_OR_POLY)?7:5].x);
						textureColorDepth = GetBits (paramFIFO[3].y, 7, 2);*/

						v[0] = paramFIFO[0];
						v[1] = paramFIFO[2];
						v[2] = paramFIFO[4];
						for (u8 i = 0; i < 3; i++) v[i].point += drawingOffset;

						if (type & RENDER_QUAD_OR_POLY) {
							v[3] = paramFIFO[6] + drawingOffset;
							Log (LOG_DEBUG_GPU, "Draw poly: X%d:Y%d - X%d:Y%d - X%d:Y%d - X%d:Y%d / TEXTURED",
								v[0].point.x, v[0].point.y,
								v[1].point.x, v[1].point.y,
								v[2].point.x, v[2].point.y,
								v[3].point.x, v[3].point.y
							);
						} else {
							Log (LOG_DEBUG_GPU, "Draw poly: X%d:Y%d - X%d:Y%d - X%d:Y%d / TEXTURED",
								v[0].point.x, v[0].point.y,
								v[1].point.x, v[1].point.y,
								v[2].point.x, v[2].point.y
							);
						}
					}

					if (!(type & RENDER_RAW_TEXTURE)) {
						Log (LOG_DEBUG_GPU, "Above texture is drawn in blended mode: 0x%08x", renderColor.To24Bit ());
					}

					/*Rect textureBoundingBox = CalculateBoundingBox ((type & RENDER_QUAD_OR_POLY)?4:3, texpageCoords);
					Vector2 textureSize = textureBoundingBox.bottomRight - textureBoundingBox.topLeft;

					// Recalculate offset from texpage Base
					switch (textureColorDepth) {
						case TEXCOLORDEPTH_4BIT: textureBoundingBox.topLeft.x /= 4; break;
						case TEXCOLORDEPTH_8BIT: textureBoundingBox.topLeft.x /= 2; break;
						case TEXCOLORDEPTH_15BIT: break;
					}

					Vector2 texpagePosition = texpageBase + textureBoundingBox.topLeft;*/
				} else {
					if (type & RENDER_SHADED) {
						v[0] = Vertex (paramFIFO[0], renderColor);
						v[1] = Vertex (paramFIFO[2], paramFIFO[1].To32 ());
						v[2] = Vertex (paramFIFO[4], paramFIFO[3].To32 ());
						for (u8 i = 0; i < 3; i++) v[i].point += drawingOffset;

						if (type & RENDER_QUAD_OR_POLY) {
							v[3] = Vertex (paramFIFO[6] + drawingOffset, paramFIFO[5].To32 ());
							Log (LOG_DEBUG_GPU, "Draw poly: X%d:Y%d (0x%08x) - X%d:Y%d (0x%08x) - X%d:Y%d (0x%08x) - X%d:Y%d (0x%08x)",
								v[0].point.x, v[0].point.y, v[0].color.To24Bit (),
								v[1].point.x, v[1].point.y, v[1].color.To24Bit (),
								v[2].point.x, v[2].point.y, v[2].color.To24Bit (),
								v[3].point.x, v[3].point.y, v[3].color.To24Bit ()
							);
						} else {
							Log (LOG_DEBUG_GPU, "Draw poly: X%d:Y%d (0x%08x) - X%d:Y%d (0x%08x) - X%d:Y%d (0x%08x)",
								v[0].point.x, v[0].point.y, v[0].color.To24Bit (),
								v[1].point.x, v[1].point.y, v[1].color.To24Bit (),
								v[2].point.x, v[2].point.y, v[2].color.To24Bit ()
							);
						}
					} else {
						v[0] = paramFIFO[0];
						v[1] = paramFIFO[1];
						v[2] = paramFIFO[2];
						for (u8 i = 0; i < 3; i++) v[i].point += drawingOffset;

						if (type & RENDER_QUAD_OR_POLY) {
							v[3] = paramFIFO[3] + drawingOffset;
							Log (LOG_DEBUG_GPU, "Draw poly: X%d:Y%d - X%d:Y%d - X%d:Y%d - X%d:Y%d",
								v[0].point.x, v[0].point.y,
								v[1].point.x, v[1].point.y,
								v[2].point.x, v[2].point.y,
								v[3].point.x, v[3].point.y
							);
						} else {
							Log (LOG_DEBUG_GPU, "Draw poly: X%d:Y%d - X%d:Y%d - X%d:Y%d",
								v[0].point.x, v[0].point.y,
								v[1].point.x, v[1].point.y,
								v[2].point.x, v[2].point.y
							);
						}
					}
				}
			} break;
			case 2: break;
			case 3: {
				//break; // TEMPORARY

				Vector2 rectPosition = paramFIFO[0];
				Vector2 rectSize;

				if (GetBit (rectPosition.x, 10)) rectPosition.x = GetBits (rectPosition.x, 0, 10) - (1 << 10);
				if (GetBit (rectPosition.y, 10)) rectPosition.y = GetBits (rectPosition.y, 0, 10) - (1 << 10);

				rectPosition += drawingOffset;

				// Rectangle size
				switch (GetBits (type, 3, 2)) {
					case 0: {
						if (type & RENDER_TEXTURED) {
							rectSize = paramFIFO[2];
						} else  {
							rectSize = paramFIFO[1];
						}
					} break;
					case 1: rectSize = Vector2 (1, 1); break;
					case 2: rectSize = Vector2 (8, 8); break;
					case 3: rectSize = Vector2 (16, 16); break;
				}

				/*if (type & RENDER_TEXTURED) {
					Vector2 CLUTCoords = ParseCLUT (paramFIFO[1].y);
					Vector2 texpageBase = ParseTexpageBase (GPUSTAT);
					Vector2 texpageOffset = ParseTexpageOffset (paramFIFO[1].x);
					u8 textureColorDepth = ParseColorDepth (GPUSTAT);

					switch (textureColorDepth) {
						case TEXCOLORDEPTH_4BIT: texpageOffset.x /= 4; break;
						case TEXCOLORDEPTH_8BIT: texpageOffset.x /= 2; break;
						case TEXCOLORDEPTH_15BIT: break;
					}

					Vector2 texpagePosition = texpageBase + texpageOffset;
					LoadTextureData (texpagePosition, rectSize, CLUTCoords, textureColorDepth, texturedRectFlip);
				}*/

				// Tweak bits to tell the poly renderer to draw a rect
				type &= ~RENDER_SHADED;
				type |= RENDER_QUAD_OR_POLY;

				while (rectPosition.x >= 1024) rectPosition.x -= 1024;
				while (rectPosition.y >= 512) rectPosition.y -= 512;
				while (rectPosition.x + rectSize.x < 0) rectPosition.x += 1024;
				while (rectPosition.y + rectSize.y < 0) rectPosition.y += 512;

				if (type & RENDER_TEXTURED) {
					if (type & RENDER_RAW_TEXTURE) {
						Log (LOG_DEBUG_GPU, "Draw rect: X%d:Y%d (size: %dx%d) / TEXTURED",
							rectPosition.x, rectPosition.y,
							rectSize.x, rectSize.y
						);
					} else {
						Log (LOG_DEBUG_GPU, "Draw rect: X%d:Y%d (size: %dx%d) / TEXTURED",
							rectPosition.x, rectPosition.y,
							rectSize.x, rectSize.y
						);
					}

					if (!(type & RENDER_RAW_TEXTURE)) {
						Log (LOG_DEBUG_GPU, "Above texture is drawn in blended mode: 0x%08x", renderColor.To24Bit ());
					}
				} else {
					Log (LOG_DEBUG_GPU, "Draw rect: X%d:Y%d (size: %dx%d) (color: 0x%08x)",
						rectPosition.x, rectPosition.y,
						rectSize.x, rectSize.y,
						renderColor.To24Bit ()
					);
				}
			} break;
		}
	}
}