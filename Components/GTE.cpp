#include "GTE.h"

GTE::GTE () {
	memset (R, 0, sizeof (R));
}

void GTE::SetReg (u8 reg, u32 value) {
	Log (LOG_DEBUG, "GTE Set %d = 0x%08x", reg, value);
	R[reg] = value;
}

u32 GTE::GetReg (u8 reg) {
	Log (LOG_DEBUG, "GTE Get %d", reg);
	return R[reg];
}