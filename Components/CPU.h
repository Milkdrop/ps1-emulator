#ifndef CPU_H
#define CPU_H
#include "../utils.h"
#include "MEM.h"

class CPU {
public:
	CPU ();
	void Link (MEM* _mem, GTE* _gte);

	void LoadROM (u32 _PC, u32 _GP, u32 _SP);
	void Step ();
	void IRQ (u8 code);

	// Status
	bool debug = 0;
	u8 halted = 0;
	u8 readyToLoadROM = 0;

private:
	void LoadDelay (u8 reg, u32 value);
	void SetReg (u8 reg, u32 value);
	void MTC0 (u8 reg, u32 value);
	void MFC0 (u8 target, u8 reg);
	void LWC2 (u8 base, u8 rt, i16 offset);
	void SWC2 (u8 base, u8 rt, i16 offset);

	void Load (u8 base, u8 rt, i16 offset, u8 size, u8 signext);
	void LoadUnaligned (u8 base, u8 rt, i16 offset, u8 right);
	void Store (u8 base, u8 rt, i16 offset, u8 size);
	void StoreUnaligned (u8 base, u8 rt, i16 offset, u8 right);

	void Add (u32 op1, u32 op2, u8 target, u8 unsign);
	void Sub (u32 op1, i32 op2, u8 target, u8 unsign);

	void Multiply (u8 rs, u8 rt, u8 unsign);
	void Divide (u8 rs, u8 rt, u8 unsign);

	void REGIMM (u8 rs, u8 rt, u16 imm);
	void Jump (u32 addr, u8 link);
	void Branch (i16 offset);

	// Components
	void CheckBIOSFunctions ();
	MEM* mem;
	GTE* gte;

	// State
	void SetBranchDelaySlot ();
	void UpdateState ();
	u8 pendingLoadDelay = 0;
	u8 loadDelayReg = 0;
	u32 loadDelayValue = 0;
	u8 exceptionTaken = 0;
	u8 inBranchDelaySlot = 0;
	u8 branchTaken = 0;

	// Debug
	void Disassemble (u32 instruction);

	// Registers
	u32 R[32];
	u32 HI = 0;
	u32 LO = 0;
	u32 PC = 0xBFC00000;
	u32 nextPC = 0xBFC00004;
	u32 instruction = 0;

	// COP0
	void Exception (u8 cause);
	void Exception (u8 cause, u32 _EPC);
	void RFE (u8 funct);

	u32 COP0[32];
	u32* SR = &COP0[12]; // System Status Register
	u32* CAUSE = &COP0[13]; // CAUSE Register
	u32* EPC = &COP0[14]; // Exception PC
};

#endif