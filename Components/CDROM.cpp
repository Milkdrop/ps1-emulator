#include "CDROM.h"

/* TODOS

- Status register bit 7 should be emulated

*/

CDROM::CDROM () {
	memset (loc, 0, sizeof (loc));
	memset (dataSeek, 0, sizeof (dataSeek));
}

void CDROM::Link (PSX* _psx) {
	psx = _psx;
}

void CDROM::Set (u8 offset, u32 value) {
	u8 index = statusRegister & 0b11;

	if (debug) Log (LOG_DEBUG_CDROM, "CD-ROM Set: %d.%d: 0x%08x", offset, index, value);

	if ((value & 0xFFFFFF00) != 0 && (value & 0xFFFFFF00) != 0xFFFFFF00)
		Log (LOG_WARN, "More than 8bits have been sent to the CD-ROM: 0x%08x", value);

	if (offset == 1) {
		switch (index) {
			case 0: CMD (value); break; // CMD
			default: Log (LOG_WARN, "Unimplemented CD-ROM SET offset %d.%d: 0x%08x", offset, index, value);
		}
	} else if (offset == 2) {
		switch (index) {
			case 0: PushToParameterFifo (value); break;
			case 1: IEnable = value; break;
			default: Log (LOG_WARN, "Unimplemented CD-ROM SET offset %d.%d: 0x%08x", offset, index, value);
		}
	} else if (offset == 3) {
		switch (index) {
			case 0: {
				if (GetBit (value, 7)) {
					if (debug) Log (LOG_DEBUG_CDROM, "Want read data bit set");
					SetBit (statusRegister, 6);
					isReadingData = 1;
					FillDMABuffer ();
				}

				if (GetBit (value, 6)) Log (LOG_WARN, "CD-ROM Request Register BFWR");
				if (GetBit (value, 5)) Log (LOG_WARN, "CD-ROM Request Register SMEN");
			} break;

			case 1: {
				// Ignore Command Start for some reason?
				u8 currentINT = IFlags & 0b11111;
				u8 acknowledgedINT = value & 0b11111;

				if (GetBit (value, 6)) {
					// Reset parameter fifo
					if (debug) Log (LOG_DEBUG_CDROM, "CD-ROM Parameter FIFO reset");
					parameterFifoIndex = 0;
					parameterFifoSize = 0;
					SetBit (statusRegister, 3);
					SetBit (statusRegister, 4);
				}
				
				if ((acknowledgedINT & currentINT) == currentINT) {
					IFlags &= ~acknowledgedINT; // Set IFlags
					ReceivedINTACK (currentINT);
				} else if (acknowledgedINT != 0) {
					Log (LOG_WARN, "Acknowledged a different interrupt than the one required: %d instead of %d", acknowledgedINT, currentINT);
				}

				// else if (debug) Log (LOG_DEBUG_CDROM, "Interrupt ignored");
			} break;

			default: Log (LOG_WARN, "Unimplemented CD-ROM SET offset %d.%d: 0x%08x", offset, index, value);
		}
	} else {
		Log (LOG_WARN, "Undefined CD-ROM SET offset: %d", offset);
	}
}

u16 CDROM::Get (u8 offset, u8 size) {
	u8 index = statusRegister & 0b11;

	if (debug) Log (LOG_DEBUG_CDROM, "CD-ROM Get: %d.%d", offset, index);

	if (offset == 1) {
		return PopResponseFifo ();
	} else if (offset == 2) {
		switch (index) {
			default: Log (LOG_WARN, "Unimplemented CD-ROM GET offset %d.%d", offset, index);
		}
	} else if (offset == 3) {
		switch (index) {
			case 0: return 0b11100000 | IEnable;
			case 1: return 0b11100000 | IFlags;
			case 2: return 0b11100000 | IEnable;
			case 3: return 0b11100000 | IFlags;
			default: Log (LOG_WARN, "Unimplemented CD-ROM GET offset %d.%d", offset, index);
		}
	} else {
		Log (LOG_WARN, "Undefined CD-ROM GET offset: %d", offset);
	}

	return 0;
}

void CDROM::CMD (u8 cmd) {
	responseFifoIndex = 0;
	responseFifoSize = 0;
	queuedINT = 0;
	queuedClockDelay = 40000;

	if (debug) Log (LOG_DEBUG_CDROM, "CD-ROM CMD: 0x%02x", cmd);

	PushToResponseFifo (stat);
	switch (cmd) {
		case 0x01: break; // Getstat - NOP
		case 0x02: Setloc (); break; // SetLoc
		case 0x06: StartRead (); break;
		case 0x09: Pause (); break;
		case 0x0A: Init (); break;
		case 0x0C: Log (LOG_WARN, "Unimplemented Demute"); break;
		case 0x0E: SetMode (PopParameterFifo ()); break;
		case 0x13: PushToResponseFifo (0); PushToResponseFifo (ToBCD (totalTracks - 1)); break; // TODO - Implement Sessions
		case 0x14: GetTD (PopParameterFifo ()); break; 
		case 0x15: {
			for (u8 i = 0; i < 3; i++) dataSeek[i] = loc[i];
			PushToResponseFifo (stat);
			queuedINT = 2;
		} break; // SeekL
		case 0x19: {
			ResetResponseFifo ();
			u8 subFunction = PopParameterFifo ();
			switch (subFunction) {
				case 0x20: PushToResponseFifo (0x940919C0, 4); break;
				default: Log (LOG_WARN, "Unimplemented CD-ROM Test sub-function: %d", subFunction);
			} 
		} break;
		case 0x1A: {
			if (CDData != NULL) {
				PushToResponseFifo (0x02002000, 4);
				PushToResponseFifo (0x53434541, 4);
				queuedINT = 2;
			} else {
				PushToResponseFifo (0x08400000, 4);
				PushToResponseFifo (0x00000000, 4);
				queuedINT = 5;
			}
		} break;
		default: Log (LOG_WARN, "Unimplemented CD-ROM Command: %xh", cmd);
	}

	SendINT (3); // INT3 + Command Start
}

void CDROM::StartRead () {
	for (u8 i = 0; i < 3; i++) dataSeek[i] = loc[i];
	isReadingData = 1;
	PushToResponseFifo (stat);
	queuedINT = 1;
	queuedClockDelay = 600000; // First sector is slower
}

void CDROM::Pause () {
	SetBit (statusRegister, 6, 0);
	isReadingData = 0;
	PushToResponseFifo (stat);
	queuedINT = 2;
	queuedClockDelay = 1000000;
}

void CDROM::FillDMABuffer () {
	if (dataSeek[0] == 0 && dataSeek[1] < 2) {
		Log (LOG_WARN, "CD-ROM Fill DMA Buffer with an offset less than 2 seconds");
	}
	
	u32 currentSector = (u32) dataSeek[0] * CD_SECTORS_PER_SECOND * 60;
	currentSector += (u32) (dataSeek[1] - 2) * CD_SECTORS_PER_SECOND + dataSeek[2];

	u32 sectorStartIndex = currentSector * CD_BYTES_PER_SECTOR;
	Log (LOG_DEBUG_CDROM, "CD-ROM Fill DMA Buffer with sector %d at 0x%08x", currentSector, sectorStartIndex);

	// TODO - Actual CD-ROM parsing, not this
	if (GetBit (mode, 5)) memcpy (DMABuffer, CDData + sectorStartIndex + 12, 0x924);
	else memcpy (DMABuffer, CDData + sectorStartIndex + 24, 0x800);

	dataSeek[2]++;
	if (dataSeek[2] >= CD_SECTORS_PER_SECOND) {
		dataSeek[1]++;
		dataSeek[2] = 0;

		if (dataSeek[1] >= 62) {
			dataSeek[0]++;
			dataSeek[1] = 2;
		}
	}

	DMABufferIndex = 0;
}

void CDROM::Setloc () {
	for (u8 i = 0; i < 3; i++) loc[i] = GetBCD (PopParameterFifo ());
	Log (LOG_DEBUG_CDROM, "CD-ROM SetLoc: %d:%d:%d", loc[0], loc[1], loc[2]);
}

void CDROM::SetMode (u8 newMode) {
	mode = newMode;
	Log (LOG_DEBUG_CDROM, "CD-ROM Set mode: 0x%02x", mode);
	if (mode != 0x80 && mode != 0x00 && mode != 0xA0) Log (LOG_WARN, "Unimplemented CD-ROM mode: 0x%02x", mode);
}

void CDROM::GetTD (u32 track) {
	if (track == 0) track = totalTracks - 1;
	track /= CD_SECTORS_PER_SECOND;
	u16 second = ToBCD (track % 60);
	u16 minute = ToBCD (track / 60);

	Log (LOG_DEBUG_CDROM, "Get TD: %d: %x:%x", track, minute, second);
	PushToResponseFifo (minute);
	PushToResponseFifo (second);
}

void CDROM::Init () {
	mode = 0;
	queuedINT = 0;
	isReadingData = 0;
	parameterFifoIndex = parameterFifoSize = 0;
	responseFifoIndex = responseFifoSize = 0;
	wantsCommandStart = 0;

	DMABufferIndex = 0;
	statusRegister = 0b00011000;
	stat = 0b00000010;

	PushToResponseFifo (stat);
	queuedINT = 2;
	queuedClockDelay = 40000;
}

void CDROM::ReceivedINTACK (u8 interrupt) {
	if (interrupt != 0 && debug) Log (LOG_DEBUG_CDROM, "CD-ROM Interrupt ACK: %d", interrupt);
	switch (interrupt) {
		case 0: break;
		case 1: if (isReadingData) {
			ResetResponseFifo ();
			queuedINT = 1;
			queuedClockDelay = 200000;
			isReadingData = 0;
		} break;
		case 2: break;
		case 3: break;
		default: Log (LOG_WARN, "CD-ROM Acknowledged Unsupported interrupt: %d", interrupt);
	}
	
	if (queuedINT != 0) SendINT (queuedINT, queuedClockDelay);
	queuedINT = 0;
	queuedClockDelay = 40000;
}

void CDROM::FinishDMA () {
	//SetBit (statusRegister, 6, 0);
	if (debug) Log (LOG_DEBUG_CDROM, "CD-ROM DMA Finished");
}

void CDROM::IRQFired () {
	if (debug) Log (LOG_DEBUG_CDROM, "IRQ%d was received by the CPU", debugLastINT);
}

void CDROM::PushToParameterFifo (u8 value) {
	parameterFifo[parameterFifoSize++] = value;

	if (parameterFifoSize == 1) {
		SetBit (statusRegister, 3, 0);
	} else if (parameterFifoSize == 16) {
		SetBit (statusRegister, 4, 0);
		Log (LOG_WARN, "parameterFifo full");
	} else if (parameterFifoSize > 16) {
		Log (LOG_WARN, "CD-ROM Parameter Fifo is overflowing!");
		parameterFifoSize = 16;
	} 
}

void CDROM::PushToResponseFifo (u32 value, u8 size) {
	SetBit (statusRegister, 5);
	value <<= (4 - size) * 8;

	for (u8 i = 0; i < size; i++) {
		responseFifo[responseFifoSize++] = (value & 0xFF000000) >> 24;
		if (responseFifoSize >= 16) {
			Log (LOG_WARN, "CD-ROM Response Fifo is full!");
			responseFifoSize = 16;
		}
		value <<= 8;
	}
}

u8 CDROM::PopResponseFifo () {
	u8 value = responseFifo[responseFifoIndex++];

	if (responseFifoIndex >= responseFifoSize) {
		responseFifoIndex = responseFifoSize;
		SetBit (statusRegister, 5, 0); 
	}

	return value;
}

u8 CDROM::PopParameterFifo () {
	u8 value = parameterFifo[parameterFifoIndex++];

	if (parameterFifoIndex >= parameterFifoSize) {
		if (debug) Log (LOG_DEBUG_CDROM, "Auto-Emptying Parameter Fifo");
		parameterFifoIndex = parameterFifoSize;
		SetBit (statusRegister, 3);
		SetBit (statusRegister, 4);
		parameterFifoIndex = parameterFifoSize = 0;
	}

	return value;
}

void CDROM::ResetResponseFifo () {
	responseFifoIndex = 0;
	responseFifoSize = 0;
}

void CDROM::SendINT (u8 value, u32 INTDelayInCycles) {
	IFlags &= ~0b11111;
	IFlags |= value & 0b11111;

	if ((IEnable & value) == value) {
		if (debug) Log (LOG_DEBUG_CDROM, "Sending INT%d to CPU", value);
		debugLastINT = value;
		psx->EnqueueIRQ (IRQ_CDROM, INTDelayInCycles);
	} else if (debug) Log (LOG_DEBUG_CDROM, "Trying to send INT%d but it's blocked by IEnable", value);
}

void CDROM::LoadCD (const char* CDFileName) {
	if (strlen (CDFileName) == 0) return;

	u32 CDSize = LoadFile (CDData, CDFileName);
	totalTracks = CDSize / CD_BYTES_PER_SECTOR;
	Log (LOG_INFO, "CD Size: %d / Tracks: %d", CDSize, totalTracks);
}