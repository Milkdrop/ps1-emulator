#ifndef GTE_H
#define GTE_H
#include "../utils.h"

class GTE {
public:
	GTE ();
	void SetReg (u8 reg, u32 value);
	u32 GetReg (u8 reg);

private:
	u32 R[64];
};

#endif