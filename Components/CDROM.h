#ifndef CDROM_H
#define CDROM_H
#include "../utils.h"
#include "../PSX.h"

class CDROM {
public:
	CDROM ();
	void Link (PSX* _psx);
	void LoadCD (const char* CDFileName);

	void Set (u8 offset, u32 value);
	u16 Get (u8 offset, u8 size = 16);
	void FinishDMA ();
	void IRQFired ();

	u8 DMABuffer[0x930];
	u16 DMABufferIndex = 0;
	u8 statusRegister = 0b00011000;
	u8 stat = 0b00000010;
	u8 IEnable = 0;
	u8 IFlags = 0b11100000;

	// Debug
	bool debug = 0;

	// Make these private
	u8 isReadingData = 0;
private:
	// CD State
	u8* CDData = NULL;
	u8 loc[3];
	u8 dataSeek[3];
	u32 totalTracks = 0;

	// Internal State
	u8 queuedINT = 0;
	u8 debugLastINT = 0;
	u32 queuedClockDelay = 40000;
	u8 mode = 0;
	u8 wantsCommandStart = 0;

	u8 parameterFifo[17];
	u8 parameterFifoIndex = 0;
	u8 parameterFifoSize = 0;

	u8 responseFifo[17];
	u8 responseFifoIndex = 0;
	u8 responseFifoSize = 0;

	void SendINT (u8 value, u32 INTDelayInCycles = 40000);
	void CMD (u8 cmd);
	void ResetResponseFifo ();

	// FIFOs
	void PushToResponseFifo (u32 value, u8 size = 1);
	void PushToParameterFifo (u8 value);
	u8 PopParameterFifo ();
	u8 PopResponseFifo ();
	
	// State
	void SetMode (u8 newMode);
	void Init ();
	void Setloc ();
	void ReceivedINTACK (u8 interrupt);

	// Read cmds
	void StartRead ();
	void FillDMABuffer ();
	void Pause ();

	// Commands
	void GetTD (u32 track);

	// Components
	PSX* psx;
};

#endif