#ifndef TIMER_H
#define TIMER_H
#include "../utils.h"
#include "../PSX.h"
#include "GPU.h"

class Timer {
public:
	Timer ();
	void Link (PSX* _psx);
	void Tick ();
	void ReadCounterMode (u8 timer);

	u16 timerValue[3];
	u16 timerMode[3];
	u16 timerTarget[3];

private:
	u8 resetTimerNextTime[3];
	u8 systemClockTick;
	PSX* psx;
};

#endif