#ifndef MEMORY_H
#define MEMORY_H
#include "../utils.h"
#include "../PSX.h"
#include "CDROM.h"
#include "GPU.h"
#include "Controller.h"
#include "Timer.h"

class MEM {
public:
	MEM ();
	void Link (PSX* _psx, GPU* _gpu, Controller* _ctrl, CDROM* _cdrom, Timer* _timer);
	void ToggleDebug ();
	
	void Memcpy (u32 addr, u8* data, u32 size);
	void Memset (u32 addr, u8 value, u32 size);
	char* GetString (u32 addr);

	u8 Get8 (u32 addr);
	u16 Get16 (u32 addr);
	u32 Get32 (u32 addr);

	void Set8 (u32 addr, u8 value);
	void Set16 (u32 addr, u16 value);
	void Set32 (u32 addr, u32 value);

	// Interrupt Status
	u32 InterruptStatus;
	u32 InterruptMask;

	// Debug
	bool debug = 0;
private:
	u32 ResolveAddr (u32 addr);
	u32 Get (u32 addr, u8 size);
	void Set (u32 addr, u32 value, u8 size);
	void UpdateDICRbit31 ();
	void DMA (u8 channel);

	// Main RAM + Some megs (for I/O etc) + 512B for Cache Control I/O
	u8 mem[0x1200000 + 512 + 4];
	u16* mem16;
	u32* mem32;
	
	// BUS Components
	PSX* psx;
	GPU* gpu;
	Controller* ctrl;
	CDROM* cdrom;
	Timer* timer;
	
	// DMA
	u32 DMAControl;
	u32 DMAInterrupt;
	u32* MADR[7];
	u32* BCR[7];
	u32* CHCR[7];
};

#endif