#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "../utils.h"
#include "../PSX.h"

class Controller {
public:
	void Link (PSX* _psx);
	void TX (u32 value);
	u8 RX ();

	u32 JOYSTAT = 0b101;
	u32 JOYMODE = 0;

	u32 GetJOYCTRL ();
	void SetJOYCTRL (u32 value);

	void IRQFired ();
	bool debug = 0;

	u16 buttonsPressed = 0xFFFF;
private:
	void ExecuteTX ();
	u32 JOYCTRL;
	u8 LastTX;
	u8 RXFifo[5];
	u8 RXIndex = 0;

	// State
	u8 lastINT = 0;
	u8 PopRX ();

	// Components
	PSX* psx;
};

#endif