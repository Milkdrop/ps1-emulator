#include "MEM.h"

const u32 BIOS_SIZE = 0x200000;

MEM::MEM () {
	memset (mem, 0, sizeof (mem));
	mem16 = (u16*) mem;
	mem32 = (u32*) mem;

	// Init DMA
	DMAControl = 0x07654321;
	u32 addr = 0x1F801080;
	addr -= 0x1F000000 - BIOS_SIZE;

	for (int i = 0; i < 7; i++) {
		MADR[i] = &(((u32*) mem)[addr >> 2]);
		BCR[i] = &(((u32*) mem)[(addr + 4) >> 2]);
		CHCR[i] = &(((u32*) mem)[(addr + 8) >> 2]);
		addr += 0x10;
	}
}

void MEM::Link (PSX* _psx, GPU* _gpu, Controller* _ctrl, CDROM* _cdrom, Timer* _timer) {
	psx = _psx;
	gpu = _gpu;
	ctrl = _ctrl;
	cdrom = _cdrom;
	timer = _timer;
}

void MEM::ToggleDebug () {
	debug = 1 - debug;
}

inline u32 MEM::Get (u32 addr, u8 size) {
	if (addr >= 0x1F000000) {
		/*switch (addr >> 4) {
			case 0x1F80108: Log (LOG_DEBUG_MEM, "DMA MDECin"); break;
			case 0x1F80109: Log (LOG_DEBUG_MEM, "DMA MDECout"); break;
			//case 0x1F8010A: Log (LOG_DEBUG_MEM, "DMA GPU"); break;
			//case 0x1F8010B: Log (LOG_DEBUG_MEM, "DMA CD-ROM"); break;
			case 0x1F8010C: Log (LOG_DEBUG_MEM, "DMA SPU"); break;
			case 0x1F8010D: Log (LOG_DEBUG_MEM, "DMA PIO"); break;
			//case 0x1F8010E: Log (LOG_DEBUG_MEM, "DMA OTC"); break;
			default: break;
		}*/

		switch (addr) {
			case 0x1F801070: return InterruptStatus;
			case 0x1F801074: return InterruptMask;

			// Timers
			case 0x1F801100: return timer->timerValue[0];
			case 0x1F801110: return timer->timerValue[1];
			case 0x1F801120: return timer->timerValue[2];

			case 0x1F801104: timer->ReadCounterMode (0); return timer->timerMode[0];
			case 0x1F801114: timer->ReadCounterMode (1); return timer->timerMode[1];
			case 0x1F801124: timer->ReadCounterMode (2); return timer->timerMode[2];

			case 0x1F801108: return timer->timerTarget[0];
			case 0x1F801118: return timer->timerTarget[1];
			case 0x1F801128: return timer->timerTarget[2];

			// OTC
			case 0x1F8010E8: return 0x10000002; 

			// CD-ROM
			case 0x1F801800: return cdrom->statusRegister;
			case 0x1F801801: return cdrom->Get (1, size);
			case 0x1F801802: return cdrom->Get (2, size);
			case 0x1F801803: return cdrom->Get (3, size);

			// CTRL - TODO
			case 0x1F801040: return ctrl->RX ();
			case 0x1F801044: return ctrl->JOYSTAT;
			case 0x1F801048: return ctrl->JOYMODE;
			case 0x1F80104A: return ctrl->GetJOYCTRL ();
			case 0x1F80104E: return 0x0088;

			// GPU
			case 0x1F801810: return gpu->GetGPUREAD ();
			case 0x1F801814: return gpu->GPUSTAT;

			case 0x1F801DAE: return 0; // SPU Status

			// DMA
			case 0x1F8010F0: return DMAControl;
			case 0x1F8010F4: return DMAInterrupt;

			default: break;
		}

		addr -= 0x1F000000 - BIOS_SIZE; // Stuff after Main RAM
	}

	switch (size) {
		case 8: return mem[addr];
		case 16: return mem16[addr >> 1];
		case 32: return mem32[addr >> 2];
		default: Log (LOG_WARN, "Invalid Get size: %d", size); return 0;
	}
}

inline void MEM::Set (u32 addr, u32 value, u8 size) {
	if (addr >= 0x1F000000) {
		/*switch (addr >> 4) {
			case 0x1F80110: Log (LOG_DEBUG, "Access Dotclock Timer"); return;
			case 0x1F80111: Log (LOG_DEBUG, "Access Horizontal Retrace Timer"); return;
			case 0x1F80112: Log (LOG_DEBUG, "Access 1/8 System Clock"); return;
			case 0x1F80108: Log (LOG_DEBUG_MEM, "DMA MDECin"); break;
			case 0x1F80109: Log (LOG_DEBUG_MEM, "DMA MDECout"); break;
			//case 0x1F8010A: Log (LOG_INFO, "DMA GPU"); break;
			//case 0x1F8010B: Log (LOG_INFO, "DMA CD-ROM"); break;
			case 0x1F8010C: Log (LOG_DEBUG_MEM, "DMA SPU"); break;
			case 0x1F8010D: Log (LOG_DEBUG_MEM, "DMA PIO"); break;
			//case 0x1F8010E: Log (LOG_INFO, "DMA OTC"); break;
			default: break;
		}*/

		switch (addr) {
			case 0x1F801070: InterruptStatus &= value; return;
			case 0x1F801074: InterruptMask = value; return;

			case 0x1F801DAE: Log (LOG_INFO, "Set SPU Status"); return;

			// Timers
			case 0x1F801100: timer->timerValue[0] = value; return;
			case 0x1F801110: timer->timerValue[1] = value; return;
			case 0x1F801120: timer->timerValue[2] = value; return;

			case 0x1F801104: timer->timerMode[0] = 0x400 | (value & 0x3FF); timer->timerValue[0] = 0; return;
			case 0x1F801114: timer->timerMode[1] = 0x400 | (value & 0x3FF); timer->timerValue[1] = 0; return;
			case 0x1F801124: timer->timerMode[2] = 0x400 | (value & 0x3FF); timer->timerValue[2] = 0; return;

			case 0x1F801108: timer->timerTarget[0] = value; return;
			case 0x1F801118: timer->timerTarget[1] = value; return;
			case 0x1F801128: timer->timerTarget[2] = value; return;

			// CD-ROM
			case 0x1F801800: cdrom->statusRegister &= ~0b11; cdrom->statusRegister |= value & 0b11; return;
			case 0x1F801801: cdrom->Set (1, value); return;
			case 0x1F801802: cdrom->Set (2, value); return;
			case 0x1F801803: cdrom->Set (3, value); return;

			// CTRL
			case 0x1F801040: ctrl->TX (value); return;
			case 0x1F801044: return; // JOYSTAT read-only
			case 0x1F801048: ctrl->JOYMODE = value; return;
			case 0x1F80104A: ctrl->SetJOYCTRL (value); return;
			case 0x1F80104E: if (value != 0x88) Log (LOG_WARN, "Setting JOY_BAUD: 0x%08x", value); return;

			// GPU
			case 0x1F801810: gpu->GP0CMD (value); return;
			case 0x1F801814: gpu->GP1CMD (value); return;

			// DMA
			case 0x1F8010F0: Log (LOG_DEBUG_MEM, "Set DMA Control: 0x%x", value); DMAControl = value; return;
			case 0x1F8010F4: Log (LOG_DEBUG_MEM, "Set DMA Interrupt: 0x%x", value); DMAInterrupt = ((DMAInterrupt & !value) & 0xFF000000) | (value & 0x00FFFFFF); UpdateDICRbit31 (); return;

			case 0x1F801088: Log (LOG_DEBUG_MEM, "Set CHCR%d: 0x%08x", 0, value); *CHCR[0] = value; DMA (0); return;
			case 0x1F801098: Log (LOG_DEBUG_MEM, "Set CHCR%d: 0x%08x", 1, value); *CHCR[1] = value; DMA (1); return;
			case 0x1F8010A8: Log (LOG_DEBUG_MEM, "Set CHCR%d: 0x%08x", 2, value); *CHCR[2] = value; DMA (2); return;
			case 0x1F8010B8: Log (LOG_DEBUG_MEM, "Set CHCR%d: 0x%08x", 3, value); *CHCR[3] = value; DMA (3); return;
			case 0x1F8010C8: Log (LOG_DEBUG_MEM, "Set CHCR%d: 0x%08x", 4, value); *CHCR[4] = value; DMA (4); return;
			case 0x1F8010D8: Log (LOG_DEBUG_MEM, "Set CHCR%d: 0x%08x", 5, value); *CHCR[5] = value; DMA (5); return;
			case 0x1F8010E8: { 
				Log (LOG_DEBUG_MEM, "Set CHCR%d: 0x%08x", 6, value);
				*CHCR[6] = value & (0b1010001 << 24);
				*CHCR[6] |= 0b10; DMA (6);
			} return;

			default: break;
		}

		addr -= 0x1F000000 - BIOS_SIZE;
	}

	switch (size) {
		case 8: mem[addr] = (u8) value; break;
		case 16: mem16[addr >> 1] = (u16) value; break;
		case 32: mem32[addr >> 2] = value; break;
		default: Log (LOG_WARN, "Invalid Set size: %d", size);
	}
}

inline void MEM::DMA (u8 ch) {
	if (!GetBit (*CHCR[ch], 24)) { Log (LOG_DEBUG_MEM, "DMA%d Not enabled", ch); return; }
	if (GetBit (*CHCR[ch], 28) && ch != 6 && ch != 3) Log (LOG_DEBUG_MEM, "Bit 28 for DMA%d transfer has been used", ch);

	if (GetBit (DMAControl, 3 + 4 * ch)) {
		u8 DMADirection = *CHCR[ch] & 0b1;

		i8 step = 4;
		if (GetBit (*CHCR[ch], 8)) Log (LOG_DEBUG_MEM, "Unimplemented DMA Chopping");
		if (GetBit (*CHCR[ch], 1)) step = -4;

		u8 syncMode = (*CHCR[ch] >> 9) & 0b11;
		*MADR[ch] &= ~0b11;

		Log (LOG_DEBUG_MEM, "Start DMA%d, CHCR: 0x%08x, MADR: 0x%08x, BCR: 0x%08x / Sync Mode: %d",
			ch, *CHCR[ch], *MADR[ch], *BCR[ch], syncMode
		);

		if (syncMode == 0) { // Check out bit 28 of CHCR
			u32 addr = *MADR[ch];
			u32 words = *BCR[ch] & 0xFFFF;
			if (words == 0) words = 0x10000;

			if (ch == 6) { // OTC
				for (u32 i = 0; i < words; i++) {
					Set32 (addr, (addr - 4) & 0xFFFFFF);
					addr += step;
				}

				Set32 (addr + 4, ((addr + 4) & 0xFF000000) | 0x00FFFFFF);
			} else if (ch == 3) {
				Memcpy (addr, cdrom->DMABuffer, words * 4);
				if (step < 0) Log (LOG_WARN, "CD-ROM DMA with negative step!");
				cdrom->FinishDMA ();
			} else switch (ch) {
				default: Log (LOG_WARN, "Unimplemented DMA%d: syncMode %d", ch, syncMode);
			}

		} else if (syncMode == 1) {
			u32 blockSize = *BCR[ch] & 0xFFFF;
			u32 blockAmount = *BCR[ch] >> 16;
			if (blockSize == 0)
				blockSize = 0x10000;

			u32 addr = *MADR[ch]; 
			for (u32 i = 0; i < blockAmount; i++) {
				for (u32 k = 0; k < blockSize; k++) {
					switch (ch) {
						case 2: {
							if (DMADirection == 0) {
								Set32 (addr, gpu->GetGPUREAD ());
							} else {
								gpu->GP0CMD (Get32 (addr));
							}
						} break;
						case 4: if (debug) Log (LOG_DEBUG_MEM, "Ignoring SPU DMA"); break; // TODO - Implement SPU DMA
						default: Log (LOG_WARN, "Unimplemented DMA%d: syncMode %d", ch, syncMode);
					}

					addr += step;
				}
			}

			*MADR[ch] = addr;
			*BCR[ch] &= 0xFFFF; // BA is set to 0

		} else if (syncMode == 2) {
			while (!(*MADR[ch] & 0x800000)) {
				u32 header = Get32 (*MADR[ch]);
				u8 entrySize = header >> 24;

				for (u16 i = 0; i < entrySize; i++) {
					*MADR[ch] += step;
					switch (ch) {
						case 2: gpu->GP0CMD (Get32 (*MADR[ch])); break;
						default: Log (LOG_WARN, "Unimplemented DMA%d: syncMode %d", ch, syncMode);
					}
				}

				*MADR[ch] &= 0xFF000000;
				*MADR[ch] |= header & 0x00FFFFFF;
			}
		} else Log (LOG_ERR, "Invalid DMA: %d %d", ch, syncMode);

		SetBit (*CHCR[ch], 24, 0);

		if (GetBit (DMAInterrupt, 15) || (GetBit (DMAInterrupt, 23) && GetBit (DMAInterrupt, 16 + ch))) {
			SetBit (DMAInterrupt, 24 + ch, 1);
			SetBit (DMAInterrupt, 31, 1);
			psx->EnqueueIRQ (IRQ_DMA);
			Log (LOG_DEBUG_MEM, "Issuing DMA%d IRQ", ch);
		} else {
			Log (LOG_DEBUG_MEM, "Not issuing DMA%d IRQ", ch);
		}
	} else {
		Log (LOG_DEBUG_MEM, "DMA%d Started but channel is disabled. DMA Control: 0x%08x, CHCR: 0x%08x", ch, DMAControl, *CHCR[ch]);
	}
}

u8 MEM::Get8 (u32 addr) {
	if (debug) Log (LOG_DEBUG_MEM, "Get8 0x%08x\n", addr);
	return Get (ResolveAddr (addr), 8);
}

u16 MEM::Get16 (u32 addr) {
	if (debug) Log (LOG_DEBUG_MEM, "Get16 0x%08x\n", addr);
	return Get (ResolveAddr (addr), 16);
}

u32 MEM::Get32 (u32 addr) {
	if (debug) Log (LOG_DEBUG_MEM, "Get32 0x%08x\n", addr);
	return Get (ResolveAddr (addr), 32);
}

void MEM::Set8 (u32 addr, u8 value) {
	if (debug) Log (LOG_DEBUG_MEM, "Set8 %xh -> 0x%08x\n", value, addr);
	Set (ResolveAddr (addr), value, 8);
}

void MEM::Set16 (u32 addr, u16 value) {
	if (debug) Log (LOG_DEBUG_MEM, "Set16 %xh -> 0x%08x\n", value, addr);
	Set (ResolveAddr (addr), value, 16);
}

void MEM::Set32 (u32 addr, u32 value) {
	if (debug) Log (LOG_DEBUG_MEM, "Set32 %xh -> 0x%08x\n", value, addr);
	Set (ResolveAddr (addr), value, 32);
}

char* MEM::GetString (u32 addr) {
	return (char*) (mem + ResolveAddr (addr));
}

inline void MEM::UpdateDICRbit31 () {
	SetBit (DMAInterrupt, 31, GetBit (DMAInterrupt, 15) || ((DMAInterrupt & 0x7F0000) & (DMAInterrupt & 0x7F000000)));
}

inline u32 MEM::ResolveAddr (u32 addr) {
	return addr & 0x1FFFFFFF;
}

void MEM::Memcpy (u32 addr, u8* data, u32 size) {
	addr = ResolveAddr (addr);
	if (addr >= 0x1F000000) addr -= 0x1F000000 - BIOS_SIZE;
	memcpy (mem + addr, data, size);
}

void MEM::Memset (u32 addr, u8 value, u32 size) {
	addr = ResolveAddr (addr);
	if (addr >= 0x1F000000) addr -= 0x1F000000 - BIOS_SIZE;
	memset (mem + addr, value, size);
}