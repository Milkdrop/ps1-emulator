#include "Timer.h"

Timer::Timer () {
	memset (timerValue, 0, sizeof (timerValue));
	memset (timerMode, 0, sizeof (timerMode));
	memset (timerTarget, 0, sizeof (timerTarget));
	memset (resetTimerNextTime, 0, sizeof (resetTimerNextTime));

	systemClockTick = 0;
}

void Timer::Link (PSX* _psx) {
	psx = _psx;
}

void Timer::Tick () {
	for (u8 i = 0; i < 3; i++) {
		u8 canIncrement = 1;
		u8 clockSource = (timerMode[i] >> 8) & 0b11;
		
		switch (i) {
			case 0: if ((clockSource == 1 || clockSource == 3) && !psx->gpu->hitDotclockSignal) canIncrement = 0; break;
			case 1: if ((clockSource == 1 || clockSource == 3) && !psx->gpu->hitHBlankSignal) canIncrement = 0; break;
			case 2: {
				systemClockTick++;
				if (systemClockTick == 8) systemClockTick = 0;
				if ((clockSource == 2 || clockSource == 3) && systemClockTick != 0) canIncrement = 0;
			} break;
			default: break;
		}

		if (!canIncrement) continue;

		if (GetBit (timerMode[i], 0)) {
			u8 syncMode = (timerMode[i] >> 1) & 0b11;
			switch (i) {
				case 0: switch (syncMode) {
 					case 0: if (psx->gpu->isInHBlank) canIncrement = 0; break;
 					case 1: if (psx->gpu->hitHBlankSignal) resetTimerNextTime[i] = 1; break;
					case 2: if (psx->gpu->isInHBlank) { if (psx->gpu->hitHBlankSignal) resetTimerNextTime[i] = 1; } else canIncrement = 0; break;
 					case 3: if (psx->gpu->isInHBlank) { SetBit (timerMode[i], 0, 0); } else canIncrement = 0; break;
 				} break;
				
				case 1: switch (syncMode) {
 					case 0: if (psx->gpu->isInVBlank) canIncrement = 0; break;
 					case 1: if (psx->gpu->hitVBlankSignal) resetTimerNextTime[i] = 1; break;
					case 2: if (psx->gpu->isInVBlank) { if (psx->gpu->hitVBlankSignal) resetTimerNextTime[i] = 1; } else canIncrement = 0; break;
 					case 3: if (psx->gpu->isInVBlank) { SetBit (timerMode[i], 0, 0); } else canIncrement = 0; break;
 				} break;
				
				case 2: switch (syncMode) {
 					case 0: canIncrement = 0; break;
 					case 1: canIncrement = 1; break;
 					case 2: canIncrement = 1; break;
 					case 3: canIncrement = 0; break;
 				} break;
			}
		}

		if (canIncrement) timerValue[i]++;
		else continue;

		//if (i == 2 && psx->debug) Log (LOG_DEBUG, "Increment timer 2: %d (Reset: %d) (Timer mode: 0x%08x, Timer target: 0x%08x", timerValue[2], resetTimerNextTime[2], timerMode[2], timerTarget[2]);

		if (resetTimerNextTime[i] == 1) {
			resetTimerNextTime[i] = 0;
			timerValue[i] = 0;
		}

		u8 IRQ = 0;
		u8 reachedTargetValue = 0;
		u8 reachedFFFF = 0;

		if (timerValue[i] == timerTarget[i]) {
			reachedTargetValue = 1;

			if (GetBit (timerMode[i], 3)) resetTimerNextTime[i] = 1;
			if (GetBit (timerMode[i], 4)) IRQ = 1;
		}

		if (timerValue[i] == 0xFFFF) {
			reachedFFFF = 1;

			if (GetBit (timerMode[i], 5)) IRQ = 1;
		}

		if (IRQ) {
			// Either "Repeatedly" mode, or one-shot + check if we can send an IRQ
			if (GetBit (timerMode[i], 6) || (!GetBit (timerMode[i], 6) && GetBit (timerMode[i], 10))) {
				SetBit (timerMode[i], 10, 0);
				SetBit (timerMode[i], 11, reachedTargetValue);
				SetBit (timerMode[i], 12, reachedFFFF);

				if (GetBit (timerMode[i], 7)) {
					Log (LOG_WARN, "Timer is using unimplemented Toggle mode");
				}

				switch (i) {
					case 0: psx->EnqueueIRQ (IRQ_TMR0); break;
					case 1: psx->EnqueueIRQ (IRQ_TMR1); break;
					case 2: psx->EnqueueIRQ (IRQ_TMR2); break;
				}
			}
		}
	}
}

void Timer::ReadCounterMode (u8 timer) {
	SetBit (timerMode[timer], 11, 0);
	SetBit (timerMode[timer], 12, 0);
}
