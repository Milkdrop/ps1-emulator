#include "CPU.h"

CPU::CPU () {
	memset (R, 0, sizeof (R));
	memset (COP0, 0, sizeof (COP0));

	COP0[15] = 0x2; // PRID
}

void CPU::Link (MEM* _mem, GTE* _gte) {
	mem = _mem;
	gte = _gte;
}

void CPU::LoadROM (u32 _PC, u32 _GP, u32 _SP) {
	R[28] = _GP;
	R[29] = _SP;
	R[30] = _SP;

	nextPC = _PC;
	PC = nextPC;

	readyToLoadROM = 0;
}

void CPU::IRQ (u8 code) {
	mem->InterruptStatus |= 1 << code;

	if (mem->InterruptMask & mem->InterruptStatus) {
		SetBit (*CAUSE, 10);
		if (GetBit (*SR, 0) && GetBit (*SR, 10)) Exception (EXCEPT_INT, PC);
	} else SetBit (*CAUSE, 10, 0);
}

void CPU::Step () {
	if (halted) return;
	if (PC & 0b11) Exception (EXCEPT_INSTR_FETCH);

	instruction = mem->Get32 (PC);

	if (debug) {
		LogNoNewline (LOG_DEBUG_CPU, "%08x (%08x): ", PC, instruction);
		Disassemble (instruction);
	}

	PC = nextPC;
	nextPC += 4;

	u8 op = instruction >> 26;
	u8 rs = (instruction >> 21) & 0x1F;
	u8 rt = (instruction >> 16) & 0x1F;

	// I-Type
	i16 imm = instruction & 0xFFFF;

	// J-Type
	u32 target = instruction & 0x3FFFFFF;

	// R-Type
	u8 rd = (instruction >> 11) & 0x1F;
	u8 shamt = (instruction >> 6) & 0x1F;
	u8 funct = instruction & 0x3F;

	R[0] = 0;

	switch (op) {
		// Load / Store
		case 0b100000: Load (rs, rt, imm, 8, 1); break; // LB
		case 0b100100: Load (rs, rt, imm, 8, 0); break; // LBU
		case 0b100001: Load (rs, rt, imm, 16, 1); break; // LH
		case 0b100101: Load (rs, rt, imm, 16, 0); break; // LHU
		case 0b100011: Load (rs, rt, imm, 32, 1); break; // LW
		case 0b100010: LoadUnaligned (rs, rt, imm, 0); break; // LWL
		case 0b100110: LoadUnaligned (rs, rt, imm, 1); break; // LWR
		case 0b101000: Store (rs, rt, imm, 8); break; // SB
		case 0b101001: Store (rs, rt, imm, 16); break; // SH
		case 0b101011: Store (rs, rt, imm, 32); break; // SW
		case 0b101010: StoreUnaligned (rs, rt, imm, 0); break; // SWL
		case 0b101110: StoreUnaligned (rs, rt, imm, 1); break; // SWR
		case 0b001111: SetReg (rt, imm << 16); break; // LUI

		case 0b000000: switch (funct) { // SPECIAL
			case 0b100000: Add (R[rs], R[rt], rd, 0); break; // ADD
			case 0b100001: Add (R[rs], R[rt], rd, 1); break; // ADDU
			case 0b100010: Sub (R[rs], R[rt], rd, 0); break; // SUB
			case 0b100011: Sub (R[rs], R[rt], rd, 1); break; // SUBU

			case 0b101010: SetReg (rd, (i32) R[rs] < (i32) R[rt]); break; // SLT
			case 0b101011: SetReg (rd, R[rs] < R[rt]); break; // SLTU

			case 0b100100: SetReg (rd, R[rs] & R[rt]); break; // AND
			case 0b100101: SetReg (rd, R[rs] | R[rt]); break; // OR
			case 0b100110: SetReg (rd, R[rs] ^ R[rt]); break; // XOR
			case 0b100111: SetReg (rd, ~(R[rs] | R[rt])); break; // NOR

			case 0b000000: SetReg (rd, R[rt] << shamt); break; // SLL
			case 0b000010: SetReg (rd, R[rt] >> shamt); break; // SRL
			case 0b000011: SetReg (rd, (i32) R[rt] >> shamt); break; // SRA
			case 0b000100: SetReg (rd, R[rt] << (R[rs] & 0b11111)); break; // SLLV
			case 0b000110: SetReg (rd, R[rt] >> (R[rs] & 0b11111)); break; // SRLV
			case 0b000111: SetReg (rd, (i32) R[rt] >> (R[rs] & 0b11111)); break; // SRAV

			case 0b011000: Multiply (rs, rt, 0); break; // MULT
			case 0b011001: Multiply (rs, rt, 1); break; // MULTIU
			case 0b011010: Divide (rs, rt, 0); break; // DIV
			case 0b011011: Divide (rs, rt, 1); break; // DIVU

			case 0b010000: SetReg (rd, HI); break; // MFHI
			case 0b010010: SetReg (rd, LO); break; // MFLO
			case 0b010001: HI = R[rs]; break; // MTHI
			case 0b010011: LO = R[rs]; break; // MTLO

			case 0b001000: SetBranchDelaySlot (); Jump (R[rs], 0); break; // JR
			case 0b001001: SetBranchDelaySlot (); Jump (R[rs], rd); break; // JALR

			case 0b001100: Exception (EXCEPT_SYSCALL); break; // SYSCALL
			case 0b001101: Exception (EXCEPT_BREAK); break; // BREAK

			default: Log (LOG_WARN, "0x%08x: Unknown ALU function: 0x%02x", PC, funct);
		} break;

		// Arithmetic
		case 0b001000: Add (R[rs], imm, rt, 0); break; // ADDI
		case 0b001001: Add (R[rs], imm, rt, 1); break; // ADDIU

		// Comparison
		case 0b001010: SetReg (rt, (i32) R[rs] < (i32) imm); break; // SLTI
		case 0b001011: SetReg (rt, R[rs] < (u32) imm); break; // SLTIU

		// Logical
		case 0b001100: SetReg (rt, R[rs] & (u16) imm); break; // ANDI
		case 0b001101: SetReg (rt, R[rs] | (u16) imm); break; // ORI
		case 0b001110: SetReg (rt, R[rs] ^ (u16) imm); break; // XORI

		// Jumps
		case 0b000010: SetBranchDelaySlot (); Jump ((PC & 0xF0000000) | (target << 2), 0); break; // J
		case 0b000011: SetBranchDelaySlot (); Jump ((PC & 0xF0000000) | (target << 2), 31); break; // JAL

		case 0b000100: SetBranchDelaySlot (); if (R[rs] == R[rt]) {Branch (imm);} break; // BEQ
		case 0b000101: SetBranchDelaySlot (); if (R[rs] != R[rt]) {Branch (imm);} break; // BNE
		case 0b000110: SetBranchDelaySlot (); if ((i32) R[rs] <= 0) {Branch (imm);} break; // BLEZ
		case 0b000111: SetBranchDelaySlot (); if ((i32) R[rs] > 0) {Branch (imm);} break; // BGTZ

		case 0b000001: SetBranchDelaySlot (); REGIMM (rs, rt, imm); break;

		// System
		case 0b010000: // COP0
		switch (rs) {
			case 0b00100: MTC0 (rd, R[rt]); break;
			case 0b00000: MFC0 (rt, rd); break; // MFC0 - Implement set w/ delay
			case 0b10000: RFE (funct); break;

			default: Log (LOG_WARN, "0x%08x: Unknown COP0 op 0x%02x", PC, rs); break;
		};
		break;

		case 0b010001: Exception (EXCEPT_COPR_UNUSABLE); break; // COP1
		case 0b010010: /*Log (LOG_INFO, "GTE instruction");*/ break; // TODO - GTE
		case 0b110010: LWC2 (rs, rt, imm); break;
		case 0b111010: SWC2 (rs, rt, imm); break;

		default: Log (LOG_WARN, "0x%08x: Unknown Opcode 0x%02x", PC, op); halted = 1; break;
	}

	CheckBIOSFunctions ();
	UpdateState ();
}

void CPU::Load (u8 base, u8 rt, i16 offset, u8 size, u8 signext) {
	u32 addr = R[base] + offset;
	u32 value = 0;

	switch (size) {
		case 8: value = signext ? (i8) mem->Get8 (addr) : mem->Get8 (addr); break;
		case 16: {
			if (addr & 0b1) { Exception (EXCEPT_ADDR_LOAD); return; }
			else value = signext ? (i16) mem->Get16 (addr) : mem->Get16 (addr);
		} break;
		case 32: {
			if (addr & 0b11) { Exception (EXCEPT_ADDR_LOAD); return; }
			else value = mem->Get32 (addr);
		} break;
	}

	LoadDelay (rt, value);
}

void CPU::LoadUnaligned (u8 base, u8 rt, i16 offset, u8 right) {
	u32 addr = R[base] + offset;
	u32 data = mem->Get32 (addr & ~0b11);
	u32 value = R[rt];

	if (pendingLoadDelay != 0 && loadDelayReg == rt) value = loadDelayValue;
	if (right == 0) {
		switch (addr & 0b11) {
			case 0: value = (value & 0x00FFFFFF) | (data << 24); break;
			case 1: value = (value & 0x0000FFFF) | (data << 16); break;
			case 2: value = (value & 0x000000FF) | (data << 8); break;
			case 3: value = data; break;
		}
	} else {
		switch (addr & 0b11) {
			case 0: value = data; break;
			case 1: value = (value & 0xFF000000) | (data >> 8); break;
			case 2: value = (value & 0xFFFF0000) | (data >> 16); break;
			case 3: value = (value & 0xFFFFFF00) | (data >> 24); break;
		}
	}

	LoadDelay (rt, value);
}

void CPU::Store (u8 base, u8 rt, i16 offset, u8 size) {
	if (GetBit (*SR, 16)) return;

	u32 addr = R[base] + offset;

	switch (size) {
		case 8: mem->Set8 (addr, R[rt]); break;
		case 16: if (addr & 0b1) Exception (EXCEPT_ADDR_STORE); else mem->Set16 (addr, R[rt]); break;
		case 32: if (addr & 0b11) Exception (EXCEPT_ADDR_STORE); else mem->Set32 (addr, R[rt]); break;
	}
}

void CPU::StoreUnaligned (u8 base, u8 rt, i16 offset, u8 right) {
	if (GetBit (*SR, 16)) return;

	u32 addr = R[base] + offset;
	u32 value = mem->Get32 (addr & ~0b11);

	if (right == 0) {
		switch (addr & 0b11) {
			case 0: value = (value & 0xFFFFFF00) | (R[rt] >> 24); break;
			case 1: value = (value & 0xFFFF0000) | (R[rt] >> 16); break;
			case 2: value = (value & 0xFF000000) | (R[rt] >> 8); break;
			case 3: value = R[rt]; break;
		}
	} else {
		switch (addr & 0b11) {
			case 0: value = R[rt]; break;
			case 1: value = (value & 0x000000FF) | (R[rt] << 8); break;
			case 2: value = (value & 0x0000FFFF) | (R[rt] << 16); break;
			case 3: value = (value & 0x00FFFFFF) | (R[rt] << 24); break;
		}
	}

	mem->Set32 (addr & ~0b11, value);
}

void CPU::Add (u32 op1, u32 op2, u8 target, u8 unsign) {
	u32 sum = op1 + op2;

	if (unsign == 0 && ((sum ^ op1) & (sum ^ op2) & 0x80000000)) Exception (EXCEPT_OVERFLOW);
	else SetReg (target, sum);
}

void CPU::Sub (u32 op1, i32 op2, u8 target, u8 unsign) {
	u32 diff = op1 - op2;

	if (unsign == 0 && ((diff ^ op1) & (op1 ^ op2) & 0x80000000)) Exception (EXCEPT_OVERFLOW);
	else SetReg (target, diff);
}

void CPU::Multiply (u8 rs, u8 rt, u8 unsign) {
	u64 result = unsign ? (u64) R[rs] * R[rt] : (i64) (i32) R[rs] * (i32) R[rt];
	HI = result >> 32;
	LO = result;
}

void CPU::Divide (u8 rs, u8 rt, u8 unsign) {
	if (R[rt] == 0) {
		LO = (unsign || (i32) R[rs] >= 0) ? -1 : 1;
		HI = R[rs];
	} else {
		if (unsign) {
			LO = R[rs] / R[rt];
			HI = R[rs] % R[rt];
		} else {
			if (R[rs] == 0x80000000 && R[rt] == 0xFFFFFFFF) {
				LO = 0x80000000;
				HI = 0;
			} else {
				LO = ((i32) R[rs]) / ((i32) R[rt]);
				HI = ((i32) R[rs]) % ((i32) R[rt]);
			}
		}
	}
}

void CPU::Jump (u32 addr, u8 link) {
	SetReg (link, nextPC);

	if (addr & 0b11) {
		COP0[8] = addr;
		Exception (EXCEPT_ADDR_LOAD, addr);
	} else {
		nextPC = addr;
		branchTaken = 2;
	}
}

void CPU::REGIMM (u8 rs, u8 rt, u16 imm) {
	if ((i32) (R[rs] ^ (rt << 31)) < 0) Branch (imm);
	if ((rt & 0x1E) == 0x10) SetReg (31, PC + 4);
}

void CPU::Branch (i16 offset) {
	branchTaken = 2;
	nextPC += (i32) offset << 2;
	nextPC -= 4;
}

void CPU::Exception (u8 cause) {
	Exception (cause, PC - 4);
}

void CPU::Exception (u8 cause, u32 _EPC) {
	if (debug) Log (LOG_DEBUG_CPU, "Exception %d (EPC: 0x%08x)", cause, _EPC);
	*EPC = _EPC;

	if (inBranchDelaySlot == 1) {
		COP0[6] = PC;
		SetBit (*CAUSE, 31, 1);
		*EPC -= 4;
	} else SetBit (*CAUSE, 31, 0);

	SetBit (*CAUSE, 30, branchTaken == 1);
	*CAUSE &= ~0b1111100;
	*CAUSE |= cause << 2;

	*SR = (*SR & 0xFFFFFFC0) | ((*SR & 0xF) << 2);
	nextPC = (GetBit (*SR, 22) ? 0xbfc00180 : 0x80000080);
	PC = nextPC;
	nextPC += 4;
}

void CPU::RFE (u8 funct) {
	if (funct == 0b010000) {
		*SR = (*SR & ~0b1111) | ((*SR & 0b111111) >> 2);
	} else {
		Exception (EXCEPT_RESERVED_INSTR);
		Log (LOG_WARN, "Unknown COP0 instr: 0x%x", funct);
	}
}

void CPU::SetReg (u8 reg, u32 value) {
	R[reg] = value;
	if (loadDelayReg == reg) pendingLoadDelay = 0;
}

void CPU::MFC0 (u8 target, u8 reg) {
	if (reg >= 32) Exception (EXCEPT_RESERVED_INSTR);
	else LoadDelay (target, COP0[reg]);
}

void CPU::MTC0 (u8 reg, u32 value) {
	switch (reg) {
		case 6: break; // JUMPDEST
		case 8: break; // BadVaddr
		case 13: COP0[reg] = (COP0[reg] & (~(0b11 << 8))) | (value & (0b11 << 8)); break; // CAUSE, bits 8-9 are R/W
		case 14: break; // EPC
		case 15: break; // PRID
		default: COP0[reg] = value;
	}
}

void CPU::LWC2 (u8 base, u8 rt, i16 offset) {
	Log (LOG_DEBUG_CPU, "Loading stuff into GTE");
	u32 addr = R[base] + offset;
	if (addr & 0b11) Exception (EXCEPT_ADDR_STORE);
	else gte->SetReg (rt, mem->Get32 (addr));
}

void CPU::SWC2 (u8 base, u8 rt, i16 offset) {
	Log (LOG_DEBUG_CPU, "Loading stuff from GTE");
	u32 addr = R[base] + offset;
	if (addr & 0b11) Exception (EXCEPT_ADDR_STORE);
	else mem->Set32 (addr, gte->GetReg (rt));
}

void CPU::LoadDelay (u8 reg, u32 value) {
	if (pendingLoadDelay != 0 && loadDelayReg != reg) R[loadDelayReg] = loadDelayValue;
	pendingLoadDelay = 2;
	loadDelayReg = reg;
	loadDelayValue = value;
}

void CPU::CheckBIOSFunctions () {
	if ((PC & 0x1FFFFFFF) == 0xA0) {
		//if (debug) Log (LOG_DEBUG_CPU, "A Function: %xh", R[9]);

		switch (R[9]) {
			case 0x1B: break; // strlen
			case 0x2F: break; // rand
			case 0x3C: printf ("%c", R[4]); break;
			case 0x3F: break; // printf
			case 0x40: Log (LOG_ERR, "System Crash A(40h)"); halted = 1; break;
			case 0x78: break; // SeekL
			case 0xA1: Log (LOG_ERR, "System boot error: 0x%08x / 0x%08x", R[4], R[5]); halted = 1; break;
			default: Log (LOG_DEBUG_CPU, "A function: %xh", R[9]);
		}
	}

	if ((PC & 0x1FFFFFFF) == 0xB0) {
		if (debug)
			Log (LOG_DEBUG_CPU, "B Function: %xh", R[9]);

		switch (R[9]) {
			case 0x07: break; // DeliverEvent
			case 0x16: break; // RFE
			case 0x17: break; // RFE
			case 0x1A: Log (LOG_ERR, "System Crash B(1Ah)"); halted = 1; break;
			case 0x20: break;
			case 0x32: break;
			case 0x3D: printf ("%c", R[4]); break;
			case 0x0B: break;
			default: Log (LOG_DEBUG_CPU, "B function: %xh", R[9]);
		}
	}
}

void CPU::UpdateState () {
	if (pendingLoadDelay > 0) {
		pendingLoadDelay--;
		if (pendingLoadDelay == 0) R[loadDelayReg] = loadDelayValue;
	}

	if (inBranchDelaySlot > 0) inBranchDelaySlot--;
	if (branchTaken > 0) branchTaken--;
	if (PC == 0x80030000) readyToLoadROM = 1;

	/*if (PC == 0x8001f10c || PC == 0x800101e4) {
		Log (LOG_INFO, "Test finished: 0x%08x", PC);
		halted = 1;
	}*/
}

void CPU::SetBranchDelaySlot () {
	inBranchDelaySlot = 2;
}

void CPU::Disassemble (u32 instruction) {
	if (instruction == 0) {
		Log (LOG_DEBUG_CPU, "NOP");
		return;
	}

	u8 op = instruction >> 26;
	u8 rs = (instruction >> 21) & 0x1F;
	u8 rt = (instruction >> 16) & 0x1F;

	// I-Type
	i16 imm = instruction & 0xFFFF;

	// J-Type
	u32 target = instruction & 0x3FFFFFF;

	// R-Type
	u8 rd = (instruction >> 11) & 0x1F;
	u8 shamt = (instruction >> 6) & 0x1F;
	u8 funct = instruction & 0x3F;

	// Helpers
	u32 loadAddr = R[rs] + imm;

	switch (op) {
		// Load / Store
		case 0b100000: Log (LOG_DEBUG_CPU, "LB 0x%08x(%xh)->R%d", loadAddr, mem->Get8 (loadAddr), rt); break;
		case 0b100100: Log (LOG_DEBUG_CPU, "LBU 0x%08x(%xh)->R%d", loadAddr, mem->Get8 (loadAddr), rt); break;
		case 0b100001: Log (LOG_DEBUG_CPU, "LH 0x%08x(%xh)->R%d", loadAddr, mem->Get16 (loadAddr), rt); break;
		case 0b100101: Log (LOG_DEBUG_CPU, "LHU 0x%08x(%xh)->R%d", loadAddr, mem->Get16 (loadAddr), rt); break;
		case 0b100011: Log (LOG_DEBUG_CPU, "LW 0x%08x(%xh)->R%d", loadAddr, mem->Get32 (loadAddr), rt); break;
		case 0b100010: Log (LOG_DEBUG_CPU, "LWL 0x%08x->R%d", loadAddr, rt); break;
		case 0b100110: Log (LOG_DEBUG_CPU, "LWR 0x%08x->R%d", loadAddr, rt); break;
		case 0b101000: Log (LOG_DEBUG_CPU, "SB R%d(%xh)->0x%08x", rt, (u8) R[rt], loadAddr); break;
		case 0b101001: Log (LOG_DEBUG_CPU, "SH R%d(%xh)->0x%08x", rt, (u16) R[rt], loadAddr); break;
		case 0b101011: Log (LOG_DEBUG_CPU, "SW R%d(%xh)->0x%08x", rt, R[rt], loadAddr); break;
		case 0b101010: Log (LOG_DEBUG_CPU, "SWL R%d(%xh)->0x%08x", rt, R[rt], loadAddr); break;
		case 0b101110: Log (LOG_DEBUG_CPU, "SWR R%d(%xh)->0x%08x", rt, R[rt], loadAddr); break;
		case 0b001111: Log (LOG_DEBUG_CPU, "LUI %xh->R%d", imm << 16, rt); break;

		case 0b000000: switch (funct) { // SPECIAL
			case 0b100000: Log (LOG_DEBUG_CPU, "ADD R%d(%xh) + R%d(%xh)->R%d", rs, R[rs], rt, R[rt], rd); break;
			case 0b100001: Log (LOG_DEBUG_CPU, "ADDU R%d(%xh) + R%d(%xh)->R%d", rs, R[rs], rt, R[rt], rd); break;
			case 0b100010: Log (LOG_DEBUG_CPU, "SUB R%d(%xh) - R%d(%xh)->R%d", rs, R[rs], rt, R[rt], rd); break;
			case 0b100011: Log (LOG_DEBUG_CPU, "SUBU R%d(%xh) - R%d(%xh)->R%d", rs, R[rs], rt, R[rt], rd); break;

			case 0b101010: Log (LOG_DEBUG_CPU, "SLT R%d(%xh) < R%d(%xh)->R%d", rs, R[rs], rt, R[rt], rd); break;
			case 0b101011: Log (LOG_DEBUG_CPU, "SLTU R%d(%xh) < R%d(%xh)->R%d", rs, R[rs], rt, R[rt], rd); break;

			case 0b100100: Log (LOG_DEBUG_CPU, "AND R%d(%xh) & R%d(%xh)->R%d", rs, R[rs], rt, R[rt], rd); break;
			case 0b100101: Log (LOG_DEBUG_CPU, "OR R%d(%xh) | R%d(%xh)->R%d", rs, R[rs], rt, R[rt], rd); break;
			case 0b100110: Log (LOG_DEBUG_CPU, "XOR R%d(%xh) ^ R%d(%xh)->R%d", rs, R[rs], rt, R[rt], rd); break;
			case 0b100111: Log (LOG_DEBUG_CPU, "NOR R%d(%xh) ~ R%d(%xh)->R%d", rs, R[rs], rt, R[rt], rd); break;

			case 0b000000: Log (LOG_DEBUG_CPU, "SLL R%d(%xh) << %d ->R%d", rt, R[rt], shamt, rd); break;
			case 0b000010: Log (LOG_DEBUG_CPU, "SRL R%d(%xh) >> %d ->R%d", rt, R[rt], shamt, rd); break;
			case 0b000011: Log (LOG_DEBUG_CPU, "SRA R%d(%xh) >> %d ->R%d", rt, R[rt], shamt, rd); break;
			case 0b000100: Log (LOG_DEBUG_CPU, "SLLV R%d(%xh) << %d ->R%d", rt, R[rt], R[rs] & 0b11111, rd); break;
			case 0b000110: Log (LOG_DEBUG_CPU, "SRLV R%d(%xh) >> %d ->R%d", rt, R[rt], R[rs] & 0b11111, rd); break;
			case 0b000111: Log (LOG_DEBUG_CPU, "SRAV R%d(%xh) >> %d ->R%d", rt, R[rt], R[rs] & 0b11111, rd); break;

			case 0b011000: Log (LOG_DEBUG_CPU, "MULT R%d(%xh) * R%d(%xh)", rs, R[rs], rt, R[rt]); break;
			case 0b011001: Log (LOG_DEBUG_CPU, "MULTU R%d(%xh) * R%d(%xh)", rs, R[rs], rt, R[rt]); break;
			case 0b011010: Log (LOG_DEBUG_CPU, "DIV R%d(%xh) / R%d(%xh)", rs, R[rs], rt, R[rt]); break;
			case 0b011011: Log (LOG_DEBUG_CPU, "DIVU R%d(%xh) / R%d(%xh)", rs, R[rs], rt, R[rt]); break;

			case 0b010000: Log (LOG_DEBUG_CPU, "MFHI R%d", rd); break;
			case 0b010010: Log (LOG_DEBUG_CPU, "MFLO R%d", rd); break;
			case 0b010001: Log (LOG_DEBUG_CPU, "MTHI R%d(%xh)", rs, R[rs]); break;
			case 0b010011: Log (LOG_DEBUG_CPU, "MTLO R%d(%xh)", rs, R[rs]); break;

			case 0b001000: Log (LOG_DEBUG_CPU, "JR R%d(0x%08x)", rs, R[rs]); break;
			case 0b001001: Log (LOG_DEBUG_CPU, "JALR(R%d) R%d(0x%08x)", rd, rs, R[rs]); break;

			case 0b001100: Log (LOG_DEBUG_CPU, "SYSCALL"); break;
			case 0b001101: Log (LOG_DEBUG_CPU, "BREAK"); break;

			default: break;
		} break;

		// Arithmetic
		case 0b001000: Log (LOG_DEBUG_CPU, "ADDI R%d(%xh) + %xh->R%d", rs, R[rs], (i32) imm, rt); break;
		case 0b001001: Log (LOG_DEBUG_CPU, "ADDIU R%d(%xh) + %xh->R%d", rs, R[rs], (i32) imm, rt); break;

		// Comparison
		case 0b001010: Log (LOG_DEBUG_CPU, "SLT R%d(%xh) < %xh->R%d", rs, R[rs], (i32) imm, rt); break;
		case 0b001011: Log (LOG_DEBUG_CPU, "SLTU R%d(%xh) < %xh->R%d", rs, R[rs], (u32) imm, rt); break;

		// Logical
		case 0b001100: Log (LOG_DEBUG_CPU, "ANDI R%d(%xh) & %xh->R%d", rs, R[rs], (u32) imm, rt); break;
		case 0b001101: Log (LOG_DEBUG_CPU, "ORI R%d(%xh) | %xh->R%d", rs, R[rs], (u32) imm, rt); break;
		case 0b001110: Log (LOG_DEBUG_CPU, "XORI R%d(%xh) ^ %xh->R%d", rs, R[rs], (u32) imm, rt); break;

		// Jumps
		case 0b000010: Log (LOG_DEBUG_CPU, "J 0x%08x", (PC & 0xF0000000) | (target << 2)); break;
		case 0b000011: Log (LOG_DEBUG_CPU, "JAL(R31) 0x%08x", (PC & 0xF0000000) | (target << 2)); break;

		case 0b000100: Log (LOG_DEBUG_CPU, "BEQ R%d(%xh) == R%d(%xh) -> 0x%08x", rs, R[rs], rt, R[rt], PC + ((i32) imm << 2)); break;
		case 0b000101: Log (LOG_DEBUG_CPU, "BNE R%d(%xh) != R%d(%xh) -> 0x%08x", rs, R[rs], rt, R[rt], PC + ((i32) imm << 2)); break;
		case 0b000110: Log (LOG_DEBUG_CPU, "BLEZ R%d(%xh) <= 0 -> 0x%08x", rs, R[rs], PC + ((i32) imm << 2)); break;
		case 0b000111: Log (LOG_DEBUG_CPU, "BGTZ R%d(%xh) > 0 -> 0x%08x", rs, R[rs], PC + ((i32) imm << 2)); break;

		case 0b000001: {
			Log (LOG_DEBUG_CPU, "REGIMM:");
			if ((i32) (R[rs] ^ (rt << 31)) < 0) Log (LOG_DEBUG_CPU, "branch -> 0x%08x", PC + ((i32) imm << 2));
			if ((rt & 0x1E) == 0x10) Log (LOG_DEBUG_CPU, "link R31");
		} break;

		// System
		case 0b010000: // COP0
		switch (rs) {
			case 0b00100: Log (LOG_DEBUG_CPU, "MTC0 R%d(%xh)->COP0%d", rt, R[rt], rd); break;
			case 0b00000: Log (LOG_DEBUG_CPU, "MFC0 COP0%d(%xh)->R%d", rd, COP0[rd], rt); break;
			case 0b10000: {
				if (funct == 0b010000) Log (LOG_DEBUG_CPU, "RFE");
				else Log (LOG_DEBUG_CPU, "Unknown RFE funct: %xh", funct);
			} break; // RFE

			default: break;
		};
		break;
		default: break;
	}
}