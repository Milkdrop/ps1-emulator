#include "utils.h"

u64 GetCurrentTime () {
	auto now = high_resolution_clock::now ();
	return duration_cast <milliseconds> (now.time_since_epoch ()).count ();
}

void Sleep (u64 ms) {
	std::this_thread::sleep_for (std::chrono::milliseconds (ms));
}

void StartThread (void (*function)()) {
	std::thread thread (function);
	thread.detach ();
}

ImGuiTextBuffer globalLog, CPULog, MEMLog, CDROMLog, GPULog, CTRLLog;

void LogNoNewline (u8 type, const char* message, ...) {
	va_list args;
	va_start (args, message);
	Log (type, 0, message, args);
	va_end (args);
}

void Log (u8 type, const char* message, ...) {
	va_list args;
	va_start (args, message);
	Log (type, 1, message, args);
	va_end (args);
}

void Log (u8 type, u8 newline, const char* message, va_list args) {
	switch (type) {
		case LOG_INFO: globalLog.append ("[INFO]: "); break;
		case LOG_WARN: globalLog.append ("[WARN]: "); break;
		case LOG_ERR: vprintf (message, args); printf ("\n"); break;
		case LOG_DEBUG: globalLog.append ("[DEBUG]: "); break;
		
		case LOG_DEBUG_CPU: CPULog.appendfv (message, args); if (newline) CPULog.append ("\n"); return;
		case LOG_DEBUG_MEM: MEMLog.appendfv (message, args); if (newline) MEMLog.append ("\n"); return;
		case LOG_DEBUG_GPU: GPULog.appendfv (message, args); if (newline) GPULog.append ("\n"); return;
		case LOG_DEBUG_CDROM: CDROMLog.appendfv (message, args); if (newline) CDROMLog.append ("\n"); return;
		case LOG_DEBUG_CTRL: CTRLLog.appendfv (message, args); if (newline) CTRLLog.append ("\n"); return;
		default: break;
	}

	if (type != LOG_ERR) {
		globalLog.appendfv (message, args);
		globalLog.append ("\n");
	}
}

// https://github.com/fendevel/Guide-to-Modern-OpenGL-Functions#detailed-messages-with-debug-output
void GLMessageCallback (GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei, const char* message, void const*) {
	const char* src_str = [source]() {
		switch (source)
		{
		case GL_DEBUG_SOURCE_API: return "API";
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM: return "WINDOW SYSTEM";
		case GL_DEBUG_SOURCE_SHADER_COMPILER: return "SHADER COMPILER";
		case GL_DEBUG_SOURCE_THIRD_PARTY: return "THIRD PARTY";
		case GL_DEBUG_SOURCE_APPLICATION: return "APPLICATION";
		case GL_DEBUG_SOURCE_OTHER: return "OTHER";
		default: return "SOURCE???";
		}
	}();

	const char* type_str = [type]() {
		switch (type)
		{
		case GL_DEBUG_TYPE_ERROR: return "ERROR";
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: return "DEPRECATED_BEHAVIOR";
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: return "UNDEFINED_BEHAVIOR";
		case GL_DEBUG_TYPE_PORTABILITY: return "PORTABILITY";
		case GL_DEBUG_TYPE_PERFORMANCE: return "PERFORMANCE";
		case GL_DEBUG_TYPE_MARKER: return "MARKER";
		case GL_DEBUG_TYPE_OTHER: return "OTHER";
		default: return "TYPE???";
		}
	}();

	const char* severity_str = [severity]() {
		switch (severity) {
		case GL_DEBUG_SEVERITY_NOTIFICATION: return "NOTIFICATION";
		case GL_DEBUG_SEVERITY_LOW: return "LOW";
		case GL_DEBUG_SEVERITY_MEDIUM: return "MEDIUM";
		case GL_DEBUG_SEVERITY_HIGH: return "HIGH";
		default: return "SEVERITY???";
		}
	}();

	Log (LOG_WARN, "%s, %s, %s, %d: %s", src_str, type_str, severity_str, id, message);
}

u32 LoadFile (u8* &buffer, const char* filename) {
	FILE* filePointer = fopen (filename, "rb");
	if (!filePointer) {
		Log (LOG_ERR, "Cannot open file: %s", filename);
		return 0;
	}

	fseek (filePointer, 0L, SEEK_END);
	u32 fileSize = ftell (filePointer);

	buffer = new u8[fileSize + 1];

	rewind (filePointer);
	fread (buffer, 1, fileSize, filePointer);
	buffer[fileSize] = '\0';

	return fileSize;
}

u32 _GLCreateShaderProgram (const char* vertShaderFilename, const char* fragShaderFilename) {
	GLuint vertexShaderID = glCreateShader (GL_VERTEX_SHADER);
	GLuint fragmentShaderID = glCreateShader (GL_FRAGMENT_SHADER);

	u8 *vertexShaderCode, *fragmentShaderCode;
	LoadFile (vertexShaderCode, vertShaderFilename);
	LoadFile (fragmentShaderCode, fragShaderFilename);

	glShaderSource (vertexShaderID, 1, ((char const **) &vertexShaderCode), NULL);
	glShaderSource (fragmentShaderID, 1, ((char const **) &fragmentShaderCode) , NULL);
	glCompileShader (vertexShaderID);
	glCompileShader (fragmentShaderID);

	u32 shaderProgram = glCreateProgram();
	glAttachShader (shaderProgram, vertexShaderID);
	glAttachShader (shaderProgram, fragmentShaderID);
	glLinkProgram (shaderProgram);

	int shaderLogSize = 0;

	glGetProgramiv (shaderProgram, GL_INFO_LOG_LENGTH, &shaderLogSize);
	if (shaderLogSize > 0) {
		Log (LOG_ERR, "Shader program issues:");
		char* shaderLog = new char[shaderLogSize];
		glGetProgramInfoLog (shaderProgram, shaderLogSize, NULL, shaderLog);
		printf ("%s\n", shaderLog);
		Log (LOG_ERR, shaderLog);
		delete shaderLog;
	}

	glGetShaderiv (vertexShaderID, GL_INFO_LOG_LENGTH, &shaderLogSize);
	if (shaderLogSize > 0) {
		Log (LOG_ERR, "Vertex shader (%s) issues:", vertShaderFilename);
		char* shaderLog = new char[shaderLogSize];
		glGetShaderInfoLog (vertexShaderID, shaderLogSize, NULL, shaderLog);
		printf ("%s\n", shaderLog);
		Log (LOG_ERR, shaderLog);
		delete shaderLog;
	}

	glGetShaderiv (fragmentShaderID, GL_INFO_LOG_LENGTH, &shaderLogSize);
	if (shaderLogSize > 0) {
		Log (LOG_ERR, "Fragment shader (%s) issues:", fragShaderFilename);
		char* shaderLog = new char[shaderLogSize];
		glGetShaderInfoLog (fragmentShaderID, shaderLogSize, NULL, shaderLog);
		printf ("%s\n", shaderLog);
		Log (LOG_ERR, shaderLog);
		delete shaderLog;
	}

	glDetachShader (shaderProgram, vertexShaderID);
	glDetachShader (shaderProgram, fragmentShaderID);
	glDeleteShader (vertexShaderID);
	glDeleteShader (fragmentShaderID); 

	Log (LOG_INFO, "New shader compiled");
	return shaderProgram;
}

u32 GetBCD (u32 value) {
	u32 returnValue = 0;
	u32 pow = 1;
	while (value > 0) {
		returnValue += pow * (value % 16);
		pow *= 10;
		value /= 16;
	}

	return returnValue;
}

u32 ToBCD (u32 value) {
	u32 returnValue = 0;
	u32 pow = 1;
	while (value > 0) {
		returnValue += pow * (value % 10);
		pow *= 16;
		value /= 10;
	}

	return returnValue;
}