#include "PSX.h"

PSX::PSX (const char* BIOSFileName, const char* _ROMFileName) {
	ROMFileName = _ROMFileName;
	
	cpu = new CPU;
	gte = new GTE;
	gpu = new GPU;
	mem = new MEM;
	cdrom = new CDROM;
	ctrl = new Controller;
	timer = new Timer;

	cpu->Link (mem, gte);
	timer->Link (this);
	mem->Link (this, gpu, ctrl, cdrom, timer);
	cdrom->Link (this);
	ctrl->Link (this);

	LoadBIOS (BIOSFileName);
	cdrom->LoadCD (ROMFileName);

	bootTime = glfwGetTime () * 1000;
	memset (IRQPending, 0, sizeof (IRQPending));
	Log (LOG_INFO, "PSX Initialized.");
}

void PSX::EnqueueIRQ (u8 code, u32 clockDelay) {
	if (IRQPending[code] == 0) IRQPending[code] = clockDelay + 1;
}

void PSX::Clock () {
	if (gpu->debugLock || freeze) {
		//Log (LOG_WARN, "GPU is in debug lock");
		return;
	}

	clocksThisSecond++;
	cpu->Step ();
	gpu->Tick ();
	timer->Tick ();
	
	for (int i = 0; i < 11; i++) {
		if (IRQPending[i] == 1) {
			switch (i) {
				case IRQ_CDROM: cdrom->IRQFired (); break;
				case IRQ_CTRL: ctrl->IRQFired (); break;
				default: break;
			}

			cpu->IRQ (i);
		}

		if (IRQPending[i] >= 1) {
			IRQPending[i]--;
			//if (debug) Log (LOG_INFO, "IRQ%d Pending: %d", i, IRQPending[i]);
		}
	}

	if (cpu->readyToLoadROM) {
		cpu->readyToLoadROM = 0;
		LoadROM ();
	}

	if (cpu->halted) {
		printf ("CPU Halted. Execution time: %lums", (u64) (glfwGetTime () * 1000) - bootTime);
		exit (0);
	}
}

void PSX::LoadBIOS (const char* BIOSFileName) {
	u8* BIOSData;
	u32 BIOSSize = LoadFile (BIOSData, BIOSFileName);
	mem->Memcpy (0x1FC00000, BIOSData, BIOSSize);
}

void PSX::LoadROM () {
	if (strlen (ROMFileName) == 0 || strcasestr (ROMFileName, ".exe") == NULL) return;

	u8* ROMData;
	u32 ROMSize = LoadFile (ROMData, ROMFileName);
	u32* ROMHeader = (u32*) ROMData;

	Log (LOG_INFO, "ROM Size: %d", ROMSize);
	Log (LOG_INFO, "ROM Header: %s", (char*) ROMData + 0x4C);

	mem->Memset (ROMHeader[0x28 >> 2], 0, ROMHeader[0x2C >> 2]);
	mem->Memcpy (ROMHeader[0x18 >> 2], ROMData + 0x800, ROMSize - 0x800);
	cpu->LoadROM (ROMHeader[0x10 >> 2], ROMHeader[0x14 >> 2], ROMHeader[0x30 >> 2] + ROMHeader[0x34 >> 2]);
}