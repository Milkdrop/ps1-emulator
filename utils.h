#ifndef UTILS_H
#define UTILS_H
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <chrono>
#include <cstring>
#include <thread>

#include <algorithm>
#include <vector>
#include "vendor/imgui/imgui.h"
#include "vendor/imgui/imgui_impl_glfw.h"
#include "vendor/imgui/imgui_impl_opengl3.h"
#include "Utils/Typedefs.h"
#include "Utils/Consts.h"
#include "Utils/Color.hpp"
#include "Utils/Vector.hpp"
using namespace std::chrono;

// Bit manipulation
static inline u8 GetBit (u32 value, u8 bit) { return (value >> bit) & 1; }
static inline void SetBit (u32 &value, u8 bit, u8 set = 1) { if (set) value |= 1 << bit; else value &= ~(1 << bit); }
static inline void SetBit (u16 &value, u8 bit, u8 set = 1) { if (set) value |= 1 << bit; else value &= ~(1 << bit); }
static inline void SetBit (u8 &value, u8 bit, u8 set = 1) { if (set) value |= 1 << bit; else value &= ~(1 << bit); }

static inline u32 GetBits (u32 value, u8 lsb, u8 size) {
	u32 mask = (0xFFFFFFFF >> (32 - size)) << lsb;
	value &= mask;
	value >>= lsb;
	return value;
}

// Helper functions
void StartThread (void (*function)());
u32 LoadFile (u8* &buffer, const char* filename);
u64 GetCurrentTime ();
void Sleep (u64 ms);
void PrintBin (u32 value, u8 size);
u32 GetBCD (u32 value);
u32 ToBCD (u32 value);
void LogNoNewline (u8 type, const char* message, ...);
void Log (u8 type, const char* message, ...);
void Log (u8 type, u8 newline, const char* message, va_list args);
void GLMessageCallback (GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, GLchar const* message, void const* user_param);
u32 _GLCreateShaderProgram (const char* vertShaderFilename, const char* fragShaderFilename);
#endif