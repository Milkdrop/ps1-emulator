#include "GUI.h"

extern ImGuiTextBuffer globalLog, CPULog, MEMLog, CDROMLog, GPULog, CTRLLog;

void GUI::Link (PSX* _psx) {
	psx = _psx;
}

void GUI::DrawScreen () {
	glClear (GL_COLOR_BUFFER_BIT);
	ImGui_ImplOpenGL3_NewFrame ();
	ImGui_ImplGlfw_NewFrame ();
	ImGui::NewFrame ();

	//ImGui::ShowDemoWindow ();
	DrawVRAM ();
	DrawDisplay ();
	//DrawCurrentTexture ();
	DrawStencilTexture ();
	DrawLogs ();
	DrawDebugWindows ();

	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	glfwSwapBuffers (GUIContext);
}

void GUI::DrawVRAM () {
	if (!ImGui::Begin ("VRAM")) { ImGui::End (); return; };
	ImGui::Image ((void*) (intptr_t) psx->gpu->outputTexture, ImVec2 (VRAM_WIDTH, VRAM_HEIGHT));
	ImGui::End ();
}

void GUI::DrawDisplay () {
	if (!ImGui::Begin ("Display")) { ImGui::End (); return; };

	// This completely ignores the GPU display ranges, but it kinda works...
	Vector2f displayTopLeft = psx->gpu->displayOffset;
	Vector2f displayBottomRight = displayTopLeft + psx->gpu->displayResolution;

	ImVec2 windowTopLeft = ImGui::GetWindowContentRegionMin ();
	ImVec2 windowBottomRight = ImGui::GetWindowContentRegionMax ();
	ImVec2 windowSize = ImVec2 (windowBottomRight.x - windowTopLeft.x, windowBottomRight.y - windowTopLeft.y);
	ImVec2 topLeftUV = ImVec2 (displayTopLeft.x / VRAM_WIDTH, displayTopLeft.y / VRAM_HEIGHT);
	ImVec2 bottomRightUV = ImVec2 (displayBottomRight.x / VRAM_WIDTH, displayBottomRight.y / VRAM_HEIGHT);
	ImGui::Image ((void*) (intptr_t) psx->gpu->outputTexture, windowSize, topLeftUV, bottomRightUV);
	ImGui::End ();
}

void GUI::DrawCurrentTexture () {
	ImGui::Begin ("Currently drawing texture");
	ImGui::Image (
		(void*) (intptr_t) psx->gpu->VRAMCopyTexture,
		ImVec2 (128, 128)
	);
	ImGui::End ();
}

void GUI::DrawStencilTexture () {
	ImGui::Begin ("Stencil buffer");
	ImGui::Image (
		(void*) (intptr_t) psx->gpu->VRAMStencilTexture,
		ImVec2 (VRAM_WIDTH, VRAM_HEIGHT)
	);
	ImGui::End ();
}

void GUI::DrawLogs () {
	if (!ImGui::Begin ("Logs")) { ImGui::End(); return; }

	if (ImGui::Button ("Clear")) globalLog.clear ();
	ImGui::SameLine ();
	ImGui::Checkbox ("Enable debug", &psx->debug);
	ImGui::SameLine ();
	ImGui::Checkbox ("FREEZE EMU", &psx->freeze);
	ImGui::Text ("%f MHz", (float) psx->clocksPreviousSecond / 1000000);

	TextChild (globalLog);
	ImGui::End();
}

void GUI::DrawDebugWindows () {
	if (ImGui::Begin ("CPU")) {
		DisplayGenericLogWindow (CPULog, &psx->cpu->debug);
	} ImGui::End();

	if (ImGui::Begin ("MEM")) { DisplayGenericLogWindow (MEMLog, &psx->mem->debug); } ImGui::End();
	if (ImGui::Begin ("Controller")) { DisplayGenericLogWindow (CTRLLog, &psx->ctrl->debug); } ImGui::End();

	if (ImGui::Begin ("GPU")) { 
		ImGui::Text ("Load Texture time: %luus", psx->gpu->loadTextureDataTime);
		ImGui::Text ("Render time: %lums", psx->gpu->renderTime);
		DisplayGenericLogWindow (GPULog, &psx->gpu->debug);
	} ImGui::End();

	if (ImGui::Begin ("CD-ROM")) {
		ImGui::Text ("Status Register: %08xh | stat: %08xh | isReadingData: %xh\nIEnable: %08xh | IFlags: %08xh",
			psx->cdrom->statusRegister, psx->cdrom->stat, psx->cdrom->isReadingData,
			psx->cdrom->IEnable, psx->cdrom->IFlags
		);

		DisplayGenericLogWindow (CDROMLog, &psx->cdrom->debug);
	} ImGui::End();
}

void GUI::DisplayGenericLogWindow (ImGuiTextBuffer &textBuffer, bool* debugSwitch) {
	if (ImGui::Button ("Clear")) textBuffer.clear ();
	ImGui::SameLine ();
	bool copy = ImGui::Button ("Copy");
	if (copy) ImGui::LogToClipboard();
	ImGui::SameLine ();
	ImGui::Checkbox ("Enable debug", debugSwitch);
	TextChild (textBuffer);
	if (copy) ImGui::LogFinish();
}

void GUI::TextChild (ImGuiTextBuffer &textBuffer) {
	ImGui::BeginChild ("scrolling");
	ImGui::PushStyleVar (ImGuiStyleVar_ItemSpacing, ImVec2 (0, 1));
	ImGui::TextUnformatted (textBuffer.begin ());
	if (ImGui::GetScrollY () >= ImGui::GetScrollMaxY () - 15) ImGui::SetScrollHereY(1.0f);
	ImGui::PopStyleVar();
	ImGui::EndChild();
}

void GUI::InitGLAndImgui () {
	if (!glfwInit ()) Log (LOG_ERR, "Cannot initialize GLFW");

	glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHint (GLFW_VISIBLE, GLFW_FALSE);
	glfwWindowHint (GLFW_VISIBLE, GLFW_TRUE);
	glfwWindowHint (GLFW_MAXIMIZED, 1);
	GUIContext = glfwCreateWindow (VRAM_WIDTH, VRAM_HEIGHT, "PSX", NULL, GPUContext);
	if (GUIContext == NULL || GPUContext == NULL) Log (LOG_ERR, "Cannot create OpenGL window");

	glfwMakeContextCurrent (GUIContext);
	if (glewInit() != GLEW_OK) Log (LOG_ERR, "Cannot initialize GLEW");

	glfwSetInputMode (GUIContext, GLFW_STICKY_KEYS, GLFW_TRUE);
    glEnable (GL_DEBUG_OUTPUT);
	glDisable (GL_DEPTH_TEST);
    glDebugMessageCallback (GLMessageCallback, nullptr);

	IMGUI_CHECKVERSION ();
	ImGui::CreateContext ();
	ImGui::StyleColorsClassic ();

	ImGui_ImplGlfw_InitForOpenGL (GUIContext, true);
	ImGui_ImplOpenGL3_Init ("#version 330 core");
}